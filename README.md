# Digital Realiyagnostic Onbard Node Executor  
or DRONE is an autopilot for flightsimulators written in Java.

This project was created for fun and to see if i was able to do it. 

This Software should be able to control any reality presented to it, but for the moment only one is implemented.


# current progress #
+ 'reality interface' for the open source flight simulator FlightGear (http://www.flightgear.org/)
+ cyclic read of configuration file (for on-the-fly PIDtuning)
+ Log output to console
+ programable flight with some simple nodes

## future plans: ##
+ additional nodes for the flight program (autoland)
+ Log output via network, to a separate Logviewer program
+ storing logs in a database
+ expand external logviewer to basestation, with flightplanning and virtual cockpit, manual control mode

+ also being able to control VTOL stuff, like helicopters

## dependencies: ##
+ rxJava (available for instance here: http://mvnrepository.com/artifact/io.reactivex/rxjava/1.1.3)