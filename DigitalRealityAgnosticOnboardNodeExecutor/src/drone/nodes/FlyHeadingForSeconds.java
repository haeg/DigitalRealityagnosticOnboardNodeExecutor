/** FlyHeadingForSeconds.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import drone.IDrone;

public class FlyHeadingForSeconds extends FlyHeading {
	private double missingSeconds = 0.0;

	public static final String PROG_NAME = "FLYHEADING_FORSECONDS";

	public FlyHeadingForSeconds(IDrone drone) {
		super(drone);
		supportedParameters.add(SECONDS);
		missingParameters.add(SECONDS);
		name = "FlyHeadingForSeconds";
	}

	@Override
	public String getStatusLine() {
		return String.format("%s: timeLeft: %2$1.1f %3$s", name, missingSeconds, AbstractNode.getBasicStatusLine(drone, targetAlt, targetSpeed, targetHeading));
	}

	@Override
	public void execute() {
		super.execute();
		missingSeconds = missingSeconds - drone.input().cycleTime().seconds();
	}

	@Override
	public boolean isFinished() {
		return missingSeconds < 0.0;
	}

	@Override
	public void setParameter(String parameter, String value) {
		switch (parameter) {
		case SECONDS:
			missingParameters.remove(SECONDS);
			missingSeconds = Double.parseDouble(value);
			getLogChannel().logInfo(String.format("duration set: %1$3.1f", missingSeconds));
			break;

		default:
			super.setParameter(parameter, value);
		}
	}
}
