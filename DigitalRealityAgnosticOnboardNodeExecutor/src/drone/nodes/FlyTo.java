/** FlyTo.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import drone.GeneralUtils;
import drone.IDrone;
import drone.logging.ILogChannel;
import drone.types.calc.Distance;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.GpsPosition;
import drone.types.io.Heading;
import drone.util.GpsMath;

public abstract class FlyTo extends AbstractNode {
	@Override
	protected ILogChannel getLogChannel() {
		return drone.getLogChannel("node.flyto");
	}
	
	protected final IDrone drone;
	protected Altitude targetAlt = Altitude.DEFAULT;
	protected Heading targetHeading = Heading.DEFAULT;
	protected AirSpeed targetSpeed = AirSpeed.DEFAULT;
	protected GpsPosition targetPosition = GpsPosition.DEFAULT;
	protected Distance currentDistance = Distance.DEFAULT;
	

	public FlyTo(IDrone drone) {
		this.drone = drone;
		supportedParameters.add(ALT);
		supportedParameters.add(SPEED);
		supportedParameters.add(TARGET_POS);

		missingParameters.add(ALT);
		missingParameters.add(SPEED);
		missingParameters.add(TARGET_POS);
	}
	
	
	@Override
	public String getStatusLine() {
		return String.format("%1s: distance: %2$1.2f %3$s", name, currentDistance.kilometers(),
				AbstractNode.getBasicStatusLine(drone, targetAlt, targetSpeed, targetHeading));
	}


	@Override
	public void execute() {
		currentDistance = GpsMath.distanceBetween(targetPosition, drone.input().position());

		Heading currentHeading = drone.input().heading();
		targetHeading = GpsMath.HeadingFromTo(drone.input().position(), targetPosition);
		
		drone.control().getAirSpeedHold().performCycle(targetSpeed);
		drone.control().getAltitudeHold().performCycle(targetAlt);
		drone.control().getHeadingHold().performCycle(targetHeading);
	}

	@Override
	public void setParameter(String parameter, String value) {
		switch (parameter) {
		case ALT:
			missingParameters.remove(ALT);
			targetAlt = GeneralUtils.createFromString(value, Altitude.class);
			getLogChannel().logInfo(String.format("altitude set: %1$4.1f", targetAlt.feet()));
			break;
		case SPEED:
			missingParameters.remove(SPEED);
			targetSpeed = GeneralUtils.createFromString(value, AirSpeed.class);
			getLogChannel().logInfo(String.format("speed set: %1$3.1f", targetSpeed.knots()));
			break;
		case TARGET_POS:
			missingParameters.remove(TARGET_POS);
			targetPosition = GeneralUtils.createFromString(value, GpsPosition.class);
			getLogChannel().logInfo(String.format("targetPos set: %s", targetPosition));
			break;
		default:
			super.setParameter(parameter, value);
		}
	}


}
