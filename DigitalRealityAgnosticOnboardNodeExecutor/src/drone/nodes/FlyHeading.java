/** FlyHeading.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import drone.GeneralUtils;
import drone.IDrone;
import drone.data.OutputMode;
import drone.logging.ILogChannel;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Heading;

public abstract class FlyHeading extends AbstractNode implements INode {
	@Override
	protected ILogChannel getLogChannel() {
		return drone.getLogChannel("node.flyheading");
	}

	protected final IDrone drone;
	
	protected Altitude targetAlt = Altitude.DEFAULT;
	protected Heading targetHeading = Heading.DEFAULT;
	protected AirSpeed targetSpeed = AirSpeed.DEFAULT;
	

	public FlyHeading(IDrone drone) {
		super();
		this.drone = drone;
		
		supportedParameters.add(ALT);
		supportedParameters.add(HEADING);
		supportedParameters.add(SPEED);
		
		missingParameters.add(ALT);
		missingParameters.add(HEADING);
		missingParameters.add(SPEED);
	}
	
	@Override
	public void execute() {
		drone.control().getAirSpeedHold().performCycle(targetSpeed);
		drone.control().getAltitudeHold().performCycle(targetAlt);
		drone.control().getHeadingHold().performCycle(targetHeading);
	}

	@Override
	public void activate() {
		super.activate();

		getLogChannel().logInfo(String.format("activating... heading: %1$3.1f alt: %2$4.1f speed: %3$3.1f", targetHeading.degrees(), targetAlt.feet(), targetSpeed.knots()));
		drone.output().aileron().mode(OutputMode.CONTROLLED);
		drone.output().elevator().mode(OutputMode.CONTROLLED);
		drone.output().throttle().mode(OutputMode.CONTROLLED);
	}

	@Override
	public void deactivate() {
		//nothing to do
	}

	@Override
	public void setParameter(String parameter, String value) {
		switch (parameter) {
		case HEADING:
			missingParameters.remove(HEADING);
			targetHeading = GeneralUtils.createFromString(value, Heading.class);
			getLogChannel().logInfo(String.format("heading set: %1$3.1f", targetHeading.degrees()));
			break;
		case ALT:
			missingParameters.remove(ALT);
			targetAlt = GeneralUtils.createFromString(value, Altitude.class);
			getLogChannel().logInfo(String.format("altitude set: %1$4.1f", targetAlt.feet()));
			break;
		case SPEED:
			missingParameters.remove(SPEED);
			targetSpeed = GeneralUtils.createFromString(value, AirSpeed.class);
			getLogChannel().logInfo(String.format("speed set: %1$3.1f", targetSpeed.knots()));
			break;
		default:
			super.setParameter(parameter, value);
		}
	}

}
