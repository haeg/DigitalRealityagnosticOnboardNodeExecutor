/** INode.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import java.util.ArrayList;
import java.util.List;

public interface INode {
	public String getName();
	public String getStatusLine();
	
	public boolean isFinished();
	public void execute();
	public void activate();
	public void deactivate();
	
	public List<String> getSupportedParameters();
	public List<String> getMissingParameters();
	public void setParameter(String parameter, String value);
	
	
	/** CONSTANTS USED IN PROGRAM*/
	/** general */
	public static final String NAME = "NAME";
	public static final String SPEED = "SPEED";
	public static final String ALT = "ALT";
	public static final String HEADING = "HEADING";
	
	/** used in conditions*/
	public static final String UNTIL_ALT = "UNTIL_ALT";
	public static final String SECONDS = "SECONDS";
	public static final String UNTIL_DIST = "UNTIL_DIST";
	
	
	/** used by Autolaunch */
	public static final String ROTATESPEED = "ROTATESPEED";
	public static final String VERTICALSPEED = "VERTICALSPEED";
	
	/** used by FlyTo/VorTo */
	public static final String TARGET_POS = "TARGET_POS";
	
	/** used by Autoland */
	public static final String GLIDE_ANGLE = "GLIDE_ANGLE";
	public static final String RW_ALT = "RW_ALT";
	
	
	
	
	
	

	public static INode NULL = new INode() {
		@Override
		public String getName() {
			return "NULL";
		}

		@Override
		public String getStatusLine() {
			return "";
		}

		@Override
		public boolean isFinished() {
			return false;
		}

		@Override
		public void execute() {
		}

		@Override
		public void activate() {
		}

		@Override
		public void deactivate() {
		}


		@Override
		public List<String> getSupportedParameters() {
			return new ArrayList<String>();
		}

		@Override
		public List<String> getMissingParameters() {
			return new ArrayList<String>();
		}

		@Override
		public void setParameter(String parameter, String value) {
		}

	};
}
