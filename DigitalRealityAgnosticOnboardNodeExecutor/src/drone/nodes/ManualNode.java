/** ManualNode.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import java.util.ArrayList;
import java.util.List;

import drone.IDrone;
import drone.data.OutputMode;
import drone.gui.ManualNodeFrame;
import drone.types.calc.VerticalSpeed;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Heading;
import drone.types.io.Pitch;
import drone.types.io.Roll;

public class ManualNode implements INode {
	public static final String PROG_NAME = "MANUAL";
	private final IDrone drone;
	private final ManualNodeFrame frame;

	public boolean speedHoldEnabled = false;
	public AirSpeed speedHoldTarget;

	public boolean rollHoldEnabled = false;
	public Roll rollHoldTarget;

	public boolean headingHoldEnabled = false;
	public Heading headingHoldTarget;

	public boolean pitchHoldEnabled = false;
	public Pitch pitchHoldTarget;

	public boolean verticalHoldEnabled = false;
	public VerticalSpeed verticalHoldTarget;

	public boolean altitudeHoldEnabled = false;
	public Altitude altitudeHoldTarget;

	public ManualNode(IDrone drone) {
		this.drone = drone;

		this.speedHoldTarget = AirSpeed.DEFAULT;
		// this.speedHold = new AirspeedHoldThrottle(drone);

		this.rollHoldTarget = Roll.DEFAULT;
		// this.rollHold = new RollHoldAileron(drone);

		this.headingHoldTarget = drone.input().heading();
		// this.headingHold = new HeadingHoldAileron(drone);

		this.pitchHoldTarget = Pitch.DEFAULT;
		// pitchHold = new PitchHoldElevator(drone);

		this.verticalHoldTarget = VerticalSpeed.DEFAULT;
		// verticalHold = new VerticalSpeedHoldElevator(drone);

		this.altitudeHoldTarget = Altitude.fromFeet(1000.0);
		// altitudeHold = new AltitudeHoldElevator(drone);

		frame = new ManualNodeFrame(this);
		frame.setVisible(true);

	}

	public IDrone getDrone() {
		return drone;
	}

	@Override
	public String getName() {
		return "ManualNode";
	}

	@Override
	public String getStatusLine() {
		return "";
	}

	@Override
	public boolean isFinished() {
		return false;
	}

	@Override
	public void execute() {
		if (speedHoldEnabled) {
			drone.control().getAirSpeedHold().performCycle(speedHoldTarget);
		}

		if (rollHoldEnabled) {
			drone.control().getRollHold().performCycle(rollHoldTarget);
		}

		if (headingHoldEnabled) {
			drone.control().getHeadingHold().performCycle(headingHoldTarget);
		}

		if (pitchHoldEnabled) {
			drone.control().getPitchHold().performCycle(pitchHoldTarget);
		}

		if (verticalHoldEnabled) {
			drone.control().getVerticalSpeedHold().performCycle(verticalHoldTarget);
		}

		if (altitudeHoldEnabled) {
			drone.control().getAltitudeHold().performCycle(altitudeHoldTarget);
		}
	}

	@Override
	public void activate() {
		drone.output().aileron().mode(OutputMode.CONTROLLED);
		drone.output().elevator().mode(OutputMode.CONTROLLED);
		drone.output().rudder().mode(OutputMode.CONTROLLED);
		drone.output().throttle().mode(OutputMode.CONTROLLED);
	}

	@Override
	public void deactivate() {

	}

	@Override
	public List<String> getSupportedParameters() {
		return new ArrayList<String>();
	}

	@Override
	public List<String> getMissingParameters() {
		return new ArrayList<String>();
	}

	@Override
	public void setParameter(String parameter, String value) {}

}
