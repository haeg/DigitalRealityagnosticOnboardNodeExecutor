/** FileNodeProvider.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import drone.IDrone;

public class FileNodeProvider implements INodeProvider {
	private static final String LOGCHANNEL = "nodeprovider";
	private final List<INode> nodes;
	private int numWarnings;
	private int numErrors;
	private INode stagingNode;
	private final IDrone drone;

	public FileNodeProvider(final File programFile, IDrone drone) {
		this.drone = drone;
		this.nodes = new ArrayList<INode>();
		nodes.add(new InitialDummyNode());

		readProgramFile(programFile);

	}

	@Override
	public INode getNode() {
		return nodes.remove(0);
	}
	
	@Override
	public int numErrors() {
		return numErrors;
	}

	@Override
	public int numWarnings() {
		return numWarnings;
	}
	
	@Override
	public boolean hasErrorsOrWarnings() {
		return (numWarnings != 0) || (numErrors != 0);
	}

	private void readProgramFile(final File programFile) {
		try (BufferedReader reader = new BufferedReader(new FileReader(programFile))) {
			while (reader.ready()) {
				String line = reader.readLine();
				if (isLineOk(line)) {
					parseLine(line);
				}
			}
		} catch (IOException e) {
			drone.getLogChannel(LOGCHANNEL).logError("during read: " + e.getMessage());
			++numErrors;
		}
	}

	private boolean isLineOk(final String line) {
		return (!line.isEmpty() && !line.startsWith("#"));
	}

	private void parseLine(final String line) {
		if (stagingNode == null && line.startsWith("NODE ")) {
			createNode(line.substring(5));
		} else if (stagingNode != null && line.equals("EXECUTE")) {
			if (!stagingNode.getMissingParameters().isEmpty()) {
				drone.getLogChannel(LOGCHANNEL)
						.logError(String.format("%s still missing parameters:", stagingNode.getName()));
				for (String param : stagingNode.getMissingParameters()) {
					drone.getLogChannel(LOGCHANNEL).logError(param);
				}
				++numErrors;
			}
			nodes.add(stagingNode);
			stagingNode = null;
		} else if (stagingNode != null) {
			addParameter(line);
		} else {
			drone.getLogChannel(LOGCHANNEL).logWarning("unexpected line: " + line);
			++numWarnings;
		}
	}

	private void addParameter(String line) {
		String[] split = line.split("=");
		if (stagingNode.getSupportedParameters().contains(split[0])) {
			stagingNode.setParameter(split[0], split[1]);
		} else {
			drone.getLogChannel(LOGCHANNEL)
					.logWarning(String.format("%s is not supported by %s", split[0], stagingNode.getName()));
			++numWarnings;
		}
	}

	private void createNode(String name) {
		switch (name) {
		case AutoLaunch.PROG_NAME:
			stagingNode = new AutoLaunch(drone);
			break;
		case AutoLaunchWithClimb.PROG_NAME:
			stagingNode = new AutoLaunchWithClimb(drone);
			break;
		case ManualNode.PROG_NAME:
			stagingNode = new ManualNode(drone);
			break;
		case FlyHeadingForSeconds.PROG_NAME:
			stagingNode = new FlyHeadingForSeconds(drone);
			break;
		case VorToUntilDistance.PROG_NAME:
			stagingNode = new VorToUntilDistance(drone);
			break;
		case FlyToUntilDistance.PROG_NAME:
			stagingNode = new FlyToUntilDistance(drone);
			break;
		case AutoLand.PROG_NAME:
			stagingNode = new AutoLand(drone);
			break;

		default:
			drone.getLogChannel(LOGCHANNEL).logError("unknown node: " + name);
			++numErrors;
			break;

		}
	}

}
