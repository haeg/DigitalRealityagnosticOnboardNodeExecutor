/** VorToUntilDistance.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import drone.GeneralUtils;
import drone.IDrone;
import drone.types.calc.Distance;

/**
 *
 */
public class VorToUntilDistance extends VorTo {
	public static final String PROG_NAME = "VORTO_UNTILDIST";

	protected Distance targetDistance = Distance.DEFAULT;

	public VorToUntilDistance(IDrone drone) {
		super(drone);

		supportedParameters.add(UNTIL_DIST);

		missingParameters.add(UNTIL_DIST);
	}

	@Override
	public String getStatusLine() {
		return String.format("%1s: distance: ( %2$1.2f / %3$1.2f ) deviation: %4$1.2f %5$s", name, currentDistance.kilometers(),
				targetDistance.kilometers(), currentDeviation,
				AbstractNode.getBasicStatusLine(drone, targetAlt, targetSpeed, targetHeading));
	}

	@Override
	public boolean isFinished() {
		return targetDistance.meters() > currentDistance.meters();
	}

	@Override
	public void deactivate() {

	}



	@Override
	public void setParameter(String parameter, String value) {
		switch (parameter) {
		case UNTIL_DIST:
			missingParameters.remove(UNTIL_DIST);
			targetDistance = GeneralUtils.createFromString(value, Distance.class);
			getLogChannel().logInfo(String.format("distance set: %1$3.1f", targetDistance.meters()));
			break;
		default:
			super.setParameter(parameter, value);
		}
	}

}
