/** INodeProvider.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

public interface INodeProvider {
	public INode getNode();
	public int numErrors();
	public int numWarnings();
	public boolean hasErrorsOrWarnings();
}
