/** InitialDummyNode.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import java.util.ArrayList;
import java.util.List;

public class InitialDummyNode implements INode{
	@Override
	public String getName() {
		return "InitialDummyNode";
	}

	@Override
	public String getStatusLine() {
		return "";
	}

	@Override
	public boolean isFinished() {
		return true;
	}

	@Override
	public void execute() {
	}

	@Override
	public void activate() {
	}

	@Override
	public void deactivate() {
	}

	@Override
	public List<String> getSupportedParameters() {
		return new ArrayList<String>();
	}

	@Override
	public List<String> getMissingParameters() {
		return new ArrayList<String>();
	}

	@Override
	public void setParameter(String parameter, String value) {
	}

}
