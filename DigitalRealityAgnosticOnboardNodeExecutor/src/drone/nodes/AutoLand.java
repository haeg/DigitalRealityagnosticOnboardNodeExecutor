package drone.nodes;

import drone.GeneralUtils;
import drone.IDrone;
import drone.logging.ILogChannel;
import drone.types.io.Altitude;
import drone.types.io.Heading;
import drone.util.GpsMath;

public class AutoLand extends VorTo {
	
	public static final String PROG_NAME = "AUTOLAND";
	@Override
	protected ILogChannel getLogChannel() {
		return drone.getLogChannel("node.autoland");
	}
	private Altitude rwAlt = Altitude.DEFAULT;
	private double glideAngle = 0.0;

	public AutoLand(IDrone drone) {
		super(drone);
		missingParameters.add(GLIDE_ANGLE);
		missingParameters.add(RW_ALT);
		
		supportedParameters.add(GLIDE_ANGLE);
		supportedParameters.add(RW_ALT);
	}

	@Override
	public void execute() {
		headingPid.pid(drone.getPersistence().getSingle("VorTo.correctionFactor", 0.0, Double.class), 0.0, 0.0);
		
		currentDistance = GpsMath.distanceBetween(targetPosition, drone.input().position());
		
		Heading headingToTarget = GpsMath.HeadingFromTo(drone.input().position(), targetPosition);
		currentDeviation = headingToTarget.errorTo(targetHeading);
		
		
		double pidOutput = headingPid.cycle(currentDeviation, drone.input().cycleTime().seconds());
		
		Heading currentTargetHeading = Heading.fromDegrees(headingToTarget.degrees() - pidOutput);
		
		Altitude glideAlt = Altitude.fromFeet(rwAlt.feet() + currentDistance.feet()*glideAngle);
		
		Altitude altToHold = Altitude.fromFeet(Math.min(glideAlt.feet(), targetAlt.feet()));
		
				
		drone.control().getAirSpeedHold().performCycle(targetSpeed);
		drone.control().getAltitudeHold().performCycle(altToHold);
		drone.control().getHeadingHold().performCycle(currentTargetHeading);
	}
	
	@Override
	public boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public void deactivate() {
	}
	
	@Override
	public void setParameter(String parameter, String value) {
		switch (parameter) {
		case RW_ALT:
			missingParameters.remove(RW_ALT);
			rwAlt = GeneralUtils.createFromString(value, Altitude.class);
			getLogChannel().logInfo(String.format("rwAlt set: %1$3.1f", rwAlt.feet()));
			break;
		case GLIDE_ANGLE:
			missingParameters.remove(GLIDE_ANGLE);
			glideAngle = GeneralUtils.createFromString(value, Double.class);
			getLogChannel().logInfo(String.format("glideAngle set: %1$4.1f", glideAngle));
			break;
		default:
			super.setParameter(parameter, value);
		}
	}

}
