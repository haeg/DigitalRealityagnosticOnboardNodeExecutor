/** VorTo.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import drone.GeneralUtils;
import drone.IDrone;
import drone.data.OutputMode;
import drone.logging.ILogChannel;
import drone.types.calc.Distance;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.GpsPosition;
import drone.types.io.Heading;
import drone.util.GpsMath;
import drone.util.PidController;

public abstract class VorTo extends AbstractNode{
	@Override
	protected ILogChannel getLogChannel() {
		return drone.getLogChannel("node.vorto");
	}
	
	protected final IDrone drone;
	protected Altitude targetAlt = Altitude.DEFAULT;
	protected Heading targetHeading = Heading.DEFAULT;
	protected AirSpeed targetSpeed = AirSpeed.DEFAULT;
	protected GpsPosition targetPosition = GpsPosition.DEFAULT;
	protected Distance currentDistance = Distance.DEFAULT;
	protected double currentDeviation = 0.0;
	
	protected final PidController headingPid;
	
	public VorTo(IDrone drone) {
		this.drone = drone;
		supportedParameters.add(ALT);
		supportedParameters.add(HEADING);
		supportedParameters.add(SPEED);
		supportedParameters.add(TARGET_POS);

		missingParameters.add(ALT);
		missingParameters.add(HEADING);
		missingParameters.add(SPEED);
		missingParameters.add(TARGET_POS);

		headingPid = new PidController().limitOutput(
				-drone.getPersistence().getSingle("VorTo.maxCorrection", 0.0, Double.class),
				drone.getPersistence().getSingle("VorTo.maxCorrection", 0.0, Double.class));
	}
	
	@Override
	public String getStatusLine() {
		return String.format("%1s: distance: %2$1.2f deviation: %3$1.2f %4$s", name, currentDistance.kilometers(),
				 currentDeviation,
				AbstractNode.getBasicStatusLine(drone, targetAlt, targetSpeed, targetHeading));
	}
	
	@Override
	public void execute() {
		headingPid.pid(drone.getPersistence().getSingle("VorTo.correctionFactor", 0.0, Double.class), 0.0, 0.0);
		
		currentDistance = GpsMath.distanceBetween(targetPosition, drone.input().position());
		
		Heading headingToTarget = GpsMath.HeadingFromTo(drone.input().position(), targetPosition);
		currentDeviation = headingToTarget.errorTo(targetHeading);
		
		
		double pidOutput = headingPid.cycle(currentDeviation, drone.input().cycleTime().seconds());
		
		Heading currentTargetHeading = Heading.fromDegrees(targetHeading.degrees() - pidOutput);
		
				
		drone.control().getAirSpeedHold().performCycle(targetSpeed);
		drone.control().getAltitudeHold().performCycle(targetAlt);
		drone.control().getHeadingHold().performCycle(currentTargetHeading);

	}
	
	@Override
	public void activate() {
		getLogChannel()
				.logInfo(String.format("activating... targetPos: %1s heading: %2$3.1f alt: %3$4.1f speed: %4$3.1f",
						targetPosition, targetHeading.degrees(), targetAlt.feet(), targetSpeed.knots()));
		drone.output().aileron().mode(OutputMode.CONTROLLED);
		drone.output().elevator().mode(OutputMode.CONTROLLED);
		drone.output().throttle().mode(OutputMode.CONTROLLED);
	}
	
	@Override
	public void setParameter(String parameter, String value) {
		switch (parameter) {
		case HEADING:
			missingParameters.remove(HEADING);
			targetHeading = GeneralUtils.createFromString(value, Heading.class);
			getLogChannel().logInfo(String.format("heading set: %1$3.1f", targetHeading.degrees()));
			break;
		case ALT:
			missingParameters.remove(ALT);
			targetAlt = GeneralUtils.createFromString(value, Altitude.class);
			getLogChannel().logInfo(String.format("altitude set: %1$4.1f", targetAlt.feet()));
			break;
		case SPEED:
			missingParameters.remove(SPEED);
			targetSpeed = GeneralUtils.createFromString(value, AirSpeed.class);
			getLogChannel().logInfo(String.format("speed set: %1$3.1f", targetSpeed.knots()));
			break;
		case TARGET_POS:
			missingParameters.remove(TARGET_POS);
			targetPosition = GeneralUtils.createFromString(value, GpsPosition.class);
			getLogChannel().logInfo(String.format("targetPos set: %s", targetPosition));
			break;
		default:
			super.setParameter(parameter, value);
		}
	}
	
}
