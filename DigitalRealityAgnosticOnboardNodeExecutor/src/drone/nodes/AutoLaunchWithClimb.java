/** AutoLaunch.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import drone.GeneralUtils;
import drone.IDrone;
import drone.data.OutputMode;
import drone.logging.ILogChannel;
import drone.types.calc.VerticalSpeed;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Elevator;
import drone.types.io.Heading;
import drone.types.io.Pitch;
import drone.types.io.Roll;
import drone.types.io.Rudder;

public class AutoLaunchWithClimb extends AbstractNode implements INode {
	@Override
	protected ILogChannel getLogChannel() {
		return drone.getLogChannel("node.autolaunch");
	}

	public static final String PROG_NAME = "AUTOLAUNCH_WITHCLIMB";
	
	private enum Phase
	{
		ROLLING, ROTATE, CLIMB
	}
		
	private final IDrone drone;
	private AirSpeed targetSpeed;	//from persistence, opt. parameter
	private AirSpeed rotateSpeed;	//from persistence, opt. parameter
	private Pitch rotateAngle;		//from persistence
	private Altitude rotateEnd;		//from persistence
	private VerticalSpeed climbVSpeed;// from persistence, opt. parameter
	
	
	private Altitude targetAltitude;//parameter
	private Heading runwayHeading;	//parameter
	private Roll runwayRoll;		//runtime
		
	private Phase currentPhase = Phase.ROLLING;
		
	public AutoLaunchWithClimb(IDrone drone) {
		this.drone = drone;	
		targetSpeed = drone.getPersistence().getSingle("AutoLaunch.targetSpeed", AirSpeed.DEFAULT, AirSpeed.class);
		rotateSpeed = drone.getPersistence().getSingle("AutoLaunch.rotateSpeed", AirSpeed.DEFAULT, AirSpeed.class);
		rotateAngle = drone.getPersistence().getSingle("AutoLaunch.rotateAngle", Pitch.DEFAULT, Pitch.class);
		rotateEnd = drone.getPersistence().getSingle("AutoLaunchUntilAltitude.rotateEnd", Altitude.DEFAULT, Altitude.class); // will be adapted in activate();
		climbVSpeed = drone.getPersistence().getSingle("AutoLaunchUntilAltitude.climbVSpeed", VerticalSpeed.DEFAULT, VerticalSpeed.class);
		
		supportedParameters.add(HEADING);
		supportedParameters.add(ALT);
		supportedParameters.add(SPEED);
		supportedParameters.add(VERTICALSPEED);
		supportedParameters.add(ROTATESPEED);
		
		missingParameters.add(HEADING);
		missingParameters.add(ALT);
		name = "AutoLaunchWithClimb";
	}

	@Override
	public String getStatusLine() {
		switch (currentPhase) {
		case ROLLING:
			return String.format("%s: %s %s", name, "ROLLING", AbstractNode.getBasicStatusLine(drone, Altitude.DEFAULT, rotateSpeed, runwayHeading));
			//return String.format("%s: ROLLING Airspeed: ( %2$3.0f / %3$3.0f ) kt", name, drone.input().airSpeed().knots(), rotateSpeed.knots());
		case ROTATE:
			return String.format("%s: %s %s", name, "ROTATE", AbstractNode.getBasicStatusLine(drone, rotateEnd, targetSpeed, runwayHeading));
			//return String.format("%s: ROTATE Alt: ( %2$3.0f / %3$3.0f ) feet", name, drone.input().altitude().feet(), rotateEnd.feet() );
		case CLIMB:
			return String.format("%s: %s %s", name, "CLIMB", AbstractNode.getBasicStatusLine(drone, targetAltitude, targetSpeed, runwayHeading));
			//return String.format("%s: CLIMB Alt: ( %2$4.0f / %3$4.0f ) feet", name, drone.input().altitude().feet(), targetAltitude.feet());
		default:
			return "ERROR";

		}
	}

	@Override
	public boolean isFinished() {
		return (targetAltitude.feet() < drone.input().altitude().feet());
	}
	
	@Override
	public void activate() {
		super.activate();
		rotateEnd = Altitude.fromFeet(rotateEnd.feet() + drone.input().altitude().feet()); // add current groundLevel
		runwayRoll = drone.input().roll();
				
		getLogChannel().logInfo(String.format("activating... rwHeading: %1$1.1f, targetAlt: %2$1.1f, targetSpeed: %3$1.1f, targetVSpeed: %4$1.1f, rotateSpeed: %5$1.1f, rotateAngle: %6$1.1f, rotateEndAlt: %7$1.1f" , runwayHeading.degrees(), targetAltitude.feet(), targetSpeed.knots(), climbVSpeed.feetPerMinute(), rotateSpeed.knots(), rotateAngle.degrees(), rotateEnd.feet()));
		enterPhaseRolling();
	}

	@Override
	public void execute() {
		if(currentPhase == Phase.ROLLING)
		{
			drone.control().getHeadingHoldRudder().performCycle(runwayHeading);
			drone.control().getAirSpeedHold().performCycle(targetSpeed);
			drone.control().getRollHold().performCycle(runwayRoll);
			
			if(rotateSpeed.knots() < drone.input().airSpeed().knots())
			{
				enterPhaseRotate();
			}
		} 
		else if(currentPhase == Phase.ROTATE)
		{
			drone.control().getHeadingHoldRudder().performCycle(runwayHeading);
			drone.control().getAirSpeedHold().performCycle(targetSpeed);
			//TODO maybe go to zero roll
			drone.control().getRollHold().performCycle(runwayRoll);
			
			drone.control().getPitchHold().performCycle(rotateAngle);
			
			if(rotateEnd.feet() < drone.input().altitude().feet() )
			{
				enterPhaseClimb();
			}
		}
		else // CLIMB
		{
			drone.control().getHeadingHold().performCycle(runwayHeading);
			drone.control().getAirSpeedHold().performCycle(targetSpeed);
			drone.control().getVerticalSpeedHold().performCycle(climbVSpeed);
		}
	}
	
	private void enterPhaseRolling()
	{
		currentPhase = Phase.ROLLING;
		
		drone.output().rudder().mode(OutputMode.CONTROLLED);
		drone.output().aileron().mode(OutputMode.CONTROLLED);
		drone.output().throttle().mode(OutputMode.CONTROLLED);
		drone.output().elevator().mode(OutputMode.FIXED);
		drone.output().elevator().fixed(Elevator.DEFAULT);
	}
	
	private void enterPhaseRotate()
	{
		currentPhase = Phase.ROTATE;
		
		drone.output().elevator().mode(OutputMode.CONTROLLED);
	}
	
	private void enterPhaseClimb()
	{
		currentPhase = Phase.CLIMB;
		
		drone.output().rudder().mode(OutputMode.FIXED);
		drone.output().rudder().fixed(Rudder.DEFAULT);
		drone.output().rudder().controlled(Rudder.DEFAULT);
		
	}

	@Override
	public void deactivate() {
	}

	@Override
	public void setParameter(String parameter, String value) {
		switch (parameter) {
		case HEADING:
			missingParameters.remove(HEADING);
			runwayHeading = GeneralUtils.createFromString(value, Heading.class);
			getLogChannel().logInfo(String.format("runwayheading set: %1$3.1f", runwayHeading.degrees()));
			break;
		case ALT:
			missingParameters.remove(ALT);
			targetAltitude = GeneralUtils.createFromString(value, Altitude.class);
			getLogChannel().logInfo(String.format("targetAltitude set: %1$4.1f", targetAltitude.feet()));
			break;
		case SPEED:
			targetSpeed = GeneralUtils.createFromString(value, AirSpeed.class);
			getLogChannel().logInfo(String.format("targetSpeed set: %1$3.1f", targetSpeed.knots()));
		case VERTICALSPEED:
			climbVSpeed = GeneralUtils.createFromString(value, VerticalSpeed.class);
			getLogChannel().logInfo(String.format("climbVSpeed set: %1$4.1f", climbVSpeed.feetPerMinute()));
			break;
		case ROTATESPEED:
			rotateSpeed = GeneralUtils.createFromString(value, AirSpeed.class);
			getLogChannel().logInfo(String.format("rotateSpeed set: %1$3.1f", rotateSpeed.knots()));			

		default:
			super.setParameter(parameter, value);
		}
	}
}
