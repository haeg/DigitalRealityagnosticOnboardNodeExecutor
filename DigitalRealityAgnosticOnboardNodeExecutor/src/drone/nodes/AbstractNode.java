/** AbstractNode.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.nodes;

import java.util.ArrayList;
import java.util.List;

import drone.IDrone;
import drone.logging.ILogChannel;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Heading;

public abstract class AbstractNode implements INode {
	protected String name = "";
	protected final List<String> missingParameters = new ArrayList<String>();
	protected final List<String> supportedParameters = new ArrayList<String>();
	
	protected abstract ILogChannel getLogChannel();
	
	public AbstractNode() {
		supportedParameters.add(NAME);
		
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<String> getSupportedParameters() {
		return supportedParameters;
	}

	@Override
	public List<String> getMissingParameters() {
		return missingParameters;
	}
	
	@Override
	public void activate() {
		if(!missingParameters.isEmpty()){
			getLogChannel().logError("parameters missing");
			for (String param : missingParameters) {
				getLogChannel().logError(param);
			}
		}
	}

	@Override
	public void setParameter(String parameter, String value) {
		if(NAME.equals(parameter))
		{
			name = value;
			getLogChannel().logInfo(String.format("name set: %s", name));
		}
		else
		{
			getLogChannel().logWarning(String.format("unknown Parameter: %s, value: %s", parameter, value));
		}
	}
	
	public static String getBasicStatusLine(IDrone drone, Altitude targetAlt, AirSpeed targetSpeed, Heading targetHeading)
	{
		return String.format(
				"alt( %1$4.1f / %2$4.1f ) speed( %3$3.1f / %4$3.1f ) heading( %5$3.1f/ %6$3.1f)",
				drone.input().altitude().feet(), targetAlt.feet(),
				drone.input().airSpeed().knots(), targetSpeed.knots(), drone.input().heading().degrees(),
				targetHeading.degrees());
	}

}
