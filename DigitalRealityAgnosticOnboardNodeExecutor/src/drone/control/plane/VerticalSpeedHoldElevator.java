/** VerticalSpeedHoldElevator.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.IDrone;
import drone.types.calc.VerticalSpeed;
import drone.types.io.AirSpeed;
import drone.types.io.Pitch;
import drone.util.PidController;

public class VerticalSpeedHoldElevator {
	public static final String LOGNAME = "control.plane.VerticalSpeedHold";
	private final IDrone drone;
	private final PidController verticalPid;

	public VerticalSpeedHoldElevator(IDrone drone) {
		this.drone = drone;

		verticalPid = new PidController(drone.getLogChannel(LOGNAME));
		verticalPid.limitOutput(-drone.getPersistence().getSingle("plane.maxPitch", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.maxPitch", 0.0, Double.class));
		setPidFromPersistence();
	}

	public VerticalSpeedHoldElevator performCycle(VerticalSpeed target) {
		// reload pid values to enable tuning on the fly
		setPidFromPersistence();
		
		//when stalling : no climbing!
		AirSpeed stallSpeed = drone.getPersistence().getSingle("plane.stallSpeed", AirSpeed.DEFAULT, AirSpeed.class);
		if(stallSpeed.knots() > drone.input().airSpeed().knots())
		{
			target = VerticalSpeed.DEFAULT;
			drone.getLogChannel(LOGNAME).logCyclic("setting Vspeed = 0, to prevent stall");
		}

		double error = target.feetPerMinute() - drone.input().verticalSpeed().feetPerMinute();
		double deltaT = drone.input().cycleTime().seconds();

		double pidOutput = verticalPid.cycle(error, deltaT);

		drone.control().getPitchHold().performCycle(Pitch.fromDegrees(pidOutput));

		return this;
	}
	
	private void setPidFromPersistence() {
		verticalPid.pid(drone.getPersistence().getSingle("plane.VerticalSpeedHoldElevator.p", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.VerticalSpeedHoldElevator.i", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.VerticalSpeedHoldElevator.d", 0.0, Double.class));
	}

}
