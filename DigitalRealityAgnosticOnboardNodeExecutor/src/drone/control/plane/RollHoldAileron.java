/** RollHoldAileron.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.GeneralUtils;
import drone.IDrone;
import drone.types.io.Aileron;
import drone.types.io.Roll;
import drone.util.PidController;

public class RollHoldAileron {
	public static final String LOGNAME = "control.plane.RollHold";

	private final IDrone drone;
	private final PidController rollPid;

	public RollHoldAileron(IDrone drone) {
		this.drone = drone;

		rollPid = new PidController(drone.getLogChannel(LOGNAME)).limitOutput(-100.0, 100.0);

		setPidFromPersistence();
	}

	public RollHoldAileron performCycle(Roll target) {
		target = Roll.fromDegrees(GeneralUtils.limit(target.degrees(),
				drone.getPersistence().getSingle("plane.maxRoll", Roll.DEFAULT, Roll.class).degrees(),
				-drone.getPersistence().getSingle("plane.maxRoll", Roll.DEFAULT, Roll.class).degrees()));

		// reload pid values to enable tuning on the fly
		setPidFromPersistence();

		double error = target.degrees() - drone.input().roll().degrees();
		double deltaT = drone.input().cycleTime().seconds();

		double pidOutput = rollPid.cycle(error, deltaT);
		pidOutput = GeneralUtils.limit(pidOutput,
				drone.getPersistence().getSingle("plane.limitAileron", 0.0, Double.class),
				-drone.getPersistence().getSingle("plane.limitAileron", 0.0, Double.class));

		drone.output().aileron().controlled(Aileron.fromPercent(pidOutput));
		return this;
	}

	private void setPidFromPersistence() {
		rollPid.pid(drone.getPersistence().getSingle("plane.RollHoldAileron.p", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.RollHoldAileron.i", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.RollHoldAileron.d", 0.0, Double.class));
	}
}
