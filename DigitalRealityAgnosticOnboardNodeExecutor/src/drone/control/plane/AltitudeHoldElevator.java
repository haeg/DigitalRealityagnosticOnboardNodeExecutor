/** AltitudeHoldElevator.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.IDrone;
import drone.types.calc.VerticalSpeed;
import drone.types.io.Altitude;
import drone.util.PidController;

public class AltitudeHoldElevator {
	public static final String LOGNAME = "control.plane.AltitudeHold";
	private final IDrone drone;
	private final PidController altitudePid;

	public AltitudeHoldElevator(IDrone drone) {
		this.drone = drone;

		altitudePid = new PidController(drone.getLogChannel(LOGNAME));
		altitudePid.limitOutput(drone.getPersistence().getSingle("plane.decendVSpeed", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.climbVSpeed", 0.0, Double.class));
		setPidFromPersistence();
	}

	public AltitudeHoldElevator performCycle(Altitude target) {
		// reload pid values to enable tuning on the fly
		setPidFromPersistence();

		double error = target.feet() - drone.input().altitude().feet();
		double deltaT = drone.input().cycleTime().seconds();

		double pidOutput = altitudePid.cycle(error, deltaT);

		drone.control().getVerticalSpeedHold().performCycle(VerticalSpeed.fromFeetPerMinute(pidOutput));

		return this;
	}

	private void setPidFromPersistence() {
		altitudePid.pid(drone.getPersistence().getSingle("plane.AltitudeHoldElevator.p", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.AltitudeHoldElevator.i", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.AltitudeHoldElevator.d", 0.0, Double.class));
	}

}
