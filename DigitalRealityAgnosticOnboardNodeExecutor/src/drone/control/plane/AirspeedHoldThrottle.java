/** AirspeedHoldThrottle.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.IDrone;
import drone.types.io.AirSpeed;
import drone.types.io.Throttle;
import drone.util.PidController;

public class AirspeedHoldThrottle {
	public static final String LOGNAME = "control.plane.AirspeedHold";

	private final IDrone drone;
	private final PidController throttlePidController;

	public AirspeedHoldThrottle(IDrone drone) {
		this.drone = drone;

		throttlePidController = new PidController(drone.getLogChannel(LOGNAME));
		throttlePidController.limitOutput(0.0, 100.0);
		// reload pid values to enable tuning on the fly
		setPidFromPersistence();
	}

	public AirspeedHoldThrottle performCycle(AirSpeed target) {
		// reload pid values to enable tuning on the fly
		setPidFromPersistence();

		double error = target.knots() - drone.input().airSpeed().knots();
		double deltaT = drone.input().cycleTime().seconds();

		double pidOutput = throttlePidController.cycle(error, deltaT);

		drone.output().throttle().controlled(Throttle.fromPercent(pidOutput));
		return this;
	}

	private void setPidFromPersistence() {
		throttlePidController.pid(drone.getPersistence().getSingle("plane.AirspeedHoldThrottle.p", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.AirspeedHoldThrottle.i", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.AirspeedHoldThrottle.d", 0.0, Double.class));
	}
}
