/** ControlBucket.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.IDrone;

public class Controls {
	private final AirspeedHoldThrottle airSpeedHold;

	private final AltitudeHoldElevator altitudeHold;
	private final VerticalSpeedHoldElevator verticalSpeedHold;
	private final PitchHoldElevator pitchHold;

	private final HeadingHoldRudder headingHoldRudder;
	private final HeadingHoldAileron headingHold;
	private final RollHoldAileron rollHold;

	public Controls(IDrone drone) {
		airSpeedHold = new AirspeedHoldThrottle(drone);

		altitudeHold = new AltitudeHoldElevator(drone);
		verticalSpeedHold = new VerticalSpeedHoldElevator(drone);
		pitchHold = new PitchHoldElevator(drone);

		headingHoldRudder = new HeadingHoldRudder(drone);
		headingHold = new HeadingHoldAileron(drone);
		rollHold = new RollHoldAileron(drone);

	}

	public AirspeedHoldThrottle getAirSpeedHold() {
		return airSpeedHold;
	}

	public AltitudeHoldElevator getAltitudeHold() {
		return altitudeHold;
	}

	public VerticalSpeedHoldElevator getVerticalSpeedHold() {
		return verticalSpeedHold;
	}

	public PitchHoldElevator getPitchHold() {
		return pitchHold;
	}

	public HeadingHoldRudder getHeadingHoldRudder() {
		return headingHoldRudder;
	}

	public HeadingHoldAileron getHeadingHold() {
		return headingHold;
	}

	public RollHoldAileron getRollHold() {
		return rollHold;
	}

}
