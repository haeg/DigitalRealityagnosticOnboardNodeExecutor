/** HeadingHoldAileron.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.IDrone;
import drone.types.io.Heading;
import drone.types.io.Roll;
import drone.util.PidController;

public class HeadingHoldAileron {
	public static final String LOGNAME = "control.plane.HeadingHold";
	private IDrone drone;
	private PidController headingPid;

	public HeadingHoldAileron(IDrone drone) {
		this.drone = drone;

		headingPid = new PidController(drone.getLogChannel(LOGNAME));
		headingPid.limitOutput(-drone.getPersistence().getSingle("plane.maxRoll", 0.0, Double.class), 
				drone.getPersistence().getSingle("plane.maxRoll", 0.0, Double.class));
		setPidFromPersistence();
	}

	public HeadingHoldAileron performCycle(Heading target) {
		// reload pid values to enable tuning on the fly
		setPidFromPersistence();

		double error = drone.input().heading().errorTo(target);
		double deltaT = drone.input().cycleTime().seconds();

		double pidOutput = headingPid.cycle(error, deltaT);

		drone.control().getRollHold().performCycle(Roll.fromDegrees(pidOutput));
		return this;
	}
	
	
	private void setPidFromPersistence() {
		headingPid.pid(drone.getPersistence().getSingle("plane.HeadingHoldAileron.p", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.HeadingHoldAileron.i", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.HeadingHoldAileron.d", 0.0, Double.class));
	}
}
