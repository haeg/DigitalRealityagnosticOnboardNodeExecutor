/** HeadingHoldAileron.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.IDrone;
import drone.types.io.Heading;
import drone.types.io.Rudder;
import drone.util.PidController;

public class HeadingHoldRudder {
	public static final String LOGNAME = "control.plane.HeadingHold";
	private final IDrone drone;
	private final PidController rudderPid;

	public HeadingHoldRudder(IDrone drone) {
		this.drone = drone;

		rudderPid = new PidController(drone.getLogChannel(LOGNAME));
		rudderPid.limitOutput(-100.0, 100.0);
		setPidFromPersistence();
	}

	public HeadingHoldRudder performCycle(Heading target) {
		// reload pid values to enable tuning on the fly
		setPidFromPersistence();

		double error = drone.input().heading().errorTo(target);
		double deltaT = drone.input().cycleTime().seconds();

		double pidOutput = rudderPid.cycle(error, deltaT);

		drone.output().rudder().controlled(Rudder.fromPercent(pidOutput));
		return this;
	}

	private void setPidFromPersistence() {
		rudderPid.pid(drone.getPersistence().getSingle("plane.HeadingHoldRudder.p", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.HeadingHoldRudder.i", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.HeadingHoldRudder.d", 0.0, Double.class));
	}
}
