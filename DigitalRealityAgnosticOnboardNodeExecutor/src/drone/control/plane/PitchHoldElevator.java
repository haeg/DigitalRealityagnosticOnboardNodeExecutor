/** PitchHoldElevator.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.control.plane;

import drone.GeneralUtils;
import drone.IDrone;
import drone.types.io.Elevator;
import drone.types.io.Pitch;
import drone.util.PidController;

public class PitchHoldElevator {
	public static final String LOGNAME = "control.plane.PitchHold";
	private final IDrone drone;
	private final PidController pitchPid;

	public PitchHoldElevator(IDrone drone) {
		this.drone = drone;

		pitchPid = new PidController(drone.getLogChannel(LOGNAME));
		pitchPid.limitOutput(-100.0, 100.0);
		setPidFromPersistence();
	}

	public PitchHoldElevator performCycle(Pitch target) {
		target = Pitch.fromDegrees(GeneralUtils.limit(target.degrees(),
				drone.getPersistence().getSingle("plane.maxPitch", Pitch.DEFAULT, Pitch.class).degrees(),
				-drone.getPersistence().getSingle("plane.maxPitch", Pitch.DEFAULT, Pitch.class).degrees()));

		// reload pid values to enable tuning on the fly
		setPidFromPersistence();

		double error = target.degrees() - drone.input().pitch().degrees();
		double deltaT = drone.input().cycleTime().seconds();

		double pidOutput = pitchPid.cycle(error, deltaT);
		pidOutput = GeneralUtils.limit(pidOutput,
				drone.getPersistence().getSingle("plane.limitElevator", 0.0, Double.class),
				-drone.getPersistence().getSingle("plane.limitElevator", 0.0, Double.class));

		drone.output().elevator().controlled(Elevator.fromPercent(pidOutput));
		return this;
	}

	private void setPidFromPersistence() {
		pitchPid.pid(drone.getPersistence().getSingle("plane.PitchHoldElevator.p", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.PitchHoldElevator.i", 0.0, Double.class),
				drone.getPersistence().getSingle("plane.PitchHoldElevator.d", 0.0, Double.class));
	}
}
