/** DronePersistence.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.persistence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import drone.GeneralUtils;
import drone.logging.ILogChannel;
import drone.types.calc.CycleTime;

//improvement: reload in extra thread
public class ReloadingFilePersistence implements IPersistence {
	private final File persistenceFile;

	private Map<String, List<String>> persistenceMap;
	private ILogChannel logChannel;

	private boolean reloadingEnabled = false;
	private double timeToReload;

	public ReloadingFilePersistence(final File persistenceFile, final ILogChannel bootLog) {

		this.persistenceFile = persistenceFile;
		persistenceMap = new HashMap<String, List<String>>();
		this.logChannel = bootLog;
		readPersistenceFile();
	}

	public void changeLogChannel(ILogChannel newLogChannel) {
		this.logChannel = newLogChannel;
		
	}

	public void addTime(final CycleTime time) {
		if (reloadingEnabled) {
			timeToReload -= time.seconds();
			if (timeToReload < 0.0) {
				readPersistenceFile();
			}
		}
	}

	@Override
	public <T> T getSingle(String key, T defaultValue, Class<T> clazz) {
		if(persistenceMap.containsKey(key))
		{
			String valueString = persistenceMap.get(key).get(0);
			T value = GeneralUtils.createFromString(valueString, clazz);
			return value;
			
		}
		else
		{
			logChannel.logWarning(String.format("%s not found, return default", key));
			return defaultValue;
		}
	}

	@Override
	public <T> List<T> getAll(String key, Class<T> clazz) {
		ArrayList<T> outputList = new ArrayList<T>();
		
		if(persistenceMap.containsKey(key))
		{
			for(String valueString : persistenceMap.get(key))
			{
				outputList.add(GeneralUtils.createFromString(valueString, clazz));
			}
		}
		else
		{
			logChannel.logWarning(String.format("%s not found, return default", key));
		}
		
		return outputList;
	}

	private void readPersistenceFile() {
		try (BufferedReader reader = new BufferedReader(new FileReader(persistenceFile))) {
			Map<String, List<String>> readLines = new HashMap<String, List<String>>();
			
			while (reader.ready()) {
				String line = reader.readLine();
				if (isLineOk(line)) {
					addLineToMap(readLines, line);
				}
			}
			
			persistenceMap = readLines;

		} catch (IOException e) {
			logChannel.logError("during file read: " + e.getMessage());
		}
		
		
		
		Integer reloadTime = getSingle("persistence.autoReload", 0, Integer.class);
		timeToReload = (double)reloadTime / 1000.0;
		reloadingEnabled = (reloadTime > 0);
		
		logChannel.logInfo(String.format("(re)loaded persistence: %s", getSingle("persistence", "ERROR", String.class)));
		if(reloadingEnabled)
		{
			logChannel.logInfo(String.format("autoreload after %1$1.1f seconds", timeToReload));
		}
	}

	private boolean isLineOk(final String line) {
		return (!line.isEmpty() && !line.startsWith("#") && line.contains("="));
	}

	private void addLineToMap(final Map<String, List<String>> map, final String line) {
		String[] splitLine = line.split("=");
		
		if(map.containsKey(splitLine[0]))
		{
			map.get(splitLine[0]).add(splitLine[1]);
		}
		else
		{
			ArrayList<String> list = new ArrayList<String>();
			list.add(splitLine[1]);
			map.put(splitLine[0], list);
		}
	}
}
