/** IPersistence.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.persistence;

import java.util.ArrayList;
import java.util.List;

public interface IPersistence {
	/**
	 * @param key key to be searched
	 * @param defaultValue default value, in case of error or not found
	 * @param clazz class of the returned Value
	 * @return first entry found with the given key. Or default in case of error
	 */
	public <T> T getSingle(String key, T defaultValue, Class<T> clazz);
	
	/**
	 * @param key key to be searched
	 * @param clazz class of the returned values
	 * @return list of all entries found with the given key. empty List in case of error or not found
	 */
	public <T> List<T> getAll(String key, Class<T> clazz);

	
	public static IPersistence NULL = new IPersistence() {
		@Override
		public <T> T getSingle(String key, T defaultValue, Class<T> clazz) {
			return defaultValue;
		}

		@Override
		public <T> List<T> getAll(String key, Class<T> clazz) {
			return new ArrayList<T>();
		}
	};
}
