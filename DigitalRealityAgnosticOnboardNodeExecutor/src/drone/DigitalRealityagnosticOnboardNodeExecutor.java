/** DigitalRealityagnosticOnboardNodeExecutor.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone;

import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

import drone.base.BaseConnection;
import drone.control.plane.Controls;
import drone.data.IInputProperties;
import drone.data.IOutputProperties;
import drone.data.PropertyTree;
import drone.gui.SubjectTableFrame;
import drone.logging.BootstrapLogChannel;
import drone.logging.ILogChannel;
import drone.logging.LogChannelProvider;
import drone.nodes.FileNodeProvider;
import drone.nodes.INodeProvider;
import drone.persistence.IPersistence;
import drone.persistence.ReloadingFilePersistence;
import drone.realityinferface.flightgear.FlightGearBinaryInterface;
import drone.realityinterface.IDroneRealityInterface;
import drone.realityinterface.IRealityInputData;
import drone.realityinterface.IRealityInterface;
import drone.realityinterface.RealityOutputData;
import drone.types.calc.CycleTime;

public class DigitalRealityagnosticOnboardNodeExecutor implements IDrone, IDroneRealityInterface {
	private final PropertyTree propertyTree;
	private final ReloadingFilePersistence persistence;
	private final IRealityInterface realityInterface;
	private final LogChannelProvider logChannels;
	private final Controls controls;
	private final BaseConnection baseConnection;
	
	private final INodeProvider nodeProvider;
	private final NodeExecutor nodeExecutor;

	public DigitalRealityagnosticOnboardNodeExecutor(final File persistenceFile, final File programFile) {
		propertyTree = new PropertyTree();
		persistence = new ReloadingFilePersistence(persistenceFile, new BootstrapLogChannel("persistence", ILogChannel.INFO));
		logChannels = new LogChannelProvider(this);
		persistence.changeLogChannel(logChannels.getLogChannel("persistence"));
		baseConnection = new BaseConnection(this);
		
		nodeProvider = new FileNodeProvider(programFile, this);
		
		if(!nodeProvider.hasErrorsOrWarnings())
		{
			controls = new Controls(this);

			realityInterface = new FlightGearBinaryInterface(this);

			nodeExecutor = new NodeExecutor(nodeProvider, this);

			JFrame subjectFrame = new SubjectTableFrame("Properties", propertyTree.displayRows());
			subjectFrame.setVisible(true);
			baseConnection.connect();
					
			realityInterface.startExecution();
		}
		else
		{
			throw new IllegalArgumentException("program invalid");
		}
	}

	/**
	 * -------------------------------------------------------------------------
	 * ---- IDrone
	 * -------------------------------------------------------------------------
	 */
	@Override
	public IOutputProperties output() {
		return propertyTree.outputs();
	}

	@Override
	public IInputProperties input() {
		return propertyTree.inputs();
	}

	@Override
	public ILogChannel getLogChannel(String name) {
		return logChannels.getLogChannel(name);
	}

	@Override
	public IPersistence getPersistence() {
		return persistence;
	}
	
	@Override
	public Controls control() {
		return controls;
	}
	
	@Override
	public String getStatusLine() {
		return nodeExecutor != null ? nodeExecutor.getStatusLine() : "";
	}

	/**
	 * -------------------------------------------------------------------------
	 * ---- IDroneRealityInterface
	 * -------------------------------------------------------------------------
	 */
	@Override
	public RealityOutputData executeCycle(IRealityInputData data, CycleTime cycleTime) {
		propertyTree.copyInputData(data, cycleTime);
		persistence.addTime(cycleTime);
		propertyTree.calculateDerivedInputData();
		
		nodeExecutor.executeStep();
		
		baseConnection.cycle(cycleTime);
		
		return propertyTree.collectOutputData();
	}

	/**
	 * -------------------------------------------------------------------------
	 * ---- EntryPoint to the Program
	 * -------------------------------------------------------------------------
	 */
	public static void main(String[] args) {

		ILogChannel bootLog = new BootstrapLogChannel("filechecks", ILogChannel.INFO);

		args = new String[] { "./other/onboardFiles/persistence_fgC172", "./other/onboardFiles/prog_helgoland" };	//EVIL HACK during development
		
		if (args.length != 2) {
			bootLog.logError("wrong number of parameters");
			bootLog.logInfo("needed parameters:");
			bootLog.logInfo("path to persistence file");
			bootLog.logInfo("path to program file");
			return;
		}

		File persistenceFile = null;
		File programFile = null;
		boolean ok = true;

		try {
			persistenceFile = new File(args[0]);
			programFile = new File(args[1]);

			if (!persistenceFile.canRead()) {
				bootLog.logError("persistence file is not readable: " + persistenceFile.getCanonicalPath());
				ok = false;
			}
			if (!programFile.canRead()) {
				bootLog.logError("program file is not readable: " + programFile.getCanonicalPath());
				ok = false;
			}
		} catch (IOException e) {
			bootLog.logError("while checking files: " + e.getMessage());
			ok = false;
		}

		if (ok) {
			bootLog.logInfo("files seem ok, starting program...");
			try{
				new DigitalRealityagnosticOnboardNodeExecutor(persistenceFile, programFile);
			} catch (IllegalArgumentException e)
			{
				bootLog.logError("could not parse program file, abort");
			}
		}
	}
}
