/** IDroneRealityInterface.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.realityinterface;

import drone.IDrone;
import drone.types.calc.CycleTime;

/**
 * additional methods provided only to Reality Interfaces
 */
public interface IDroneRealityInterface extends IDrone{
	/**
	 * Asks the DRONE system to react to the new data.
	 * 
	 * @param data new Data from the reality
	 * @param cycleTime time since the last cycle
	 * @return data to be send to the reality
	 */
	public RealityOutputData executeCycle(final IRealityInputData data, final CycleTime cycleTime);
}
