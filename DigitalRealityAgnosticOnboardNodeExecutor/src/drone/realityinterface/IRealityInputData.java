/** ICyclicInputData.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.realityinterface;

import drone.types.io.Aileron;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Elevator;
import drone.types.io.Fuel;
import drone.types.io.GpsPosition;
import drone.types.io.Heading;
import drone.types.io.Pitch;
import drone.types.io.Roll;
import drone.types.io.Rudder;
import drone.types.io.Throttle;
/**
 * Data to be received from the realit by a Reality Interface
 */
public interface IRealityInputData {
	public Throttle getThrottle();
	public Elevator getElevator();
	public Aileron getAileron();
	public Rudder getRudder();
	
	public AirSpeed getAirSpeed();
	public Altitude getAltitude();
	public GpsPosition getPosition();
	
	public Fuel getFuel();
	
	public Pitch getPitch();
	public Roll getRoll();
	public Heading getHeading();
}
