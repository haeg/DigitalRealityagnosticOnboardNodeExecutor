/** CyclicOutputData.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.realityinterface;

import drone.types.io.Aileron;
import drone.types.io.Brake;
import drone.types.io.Elevator;
import drone.types.io.Flaps;
import drone.types.io.Rudder;
import drone.types.io.Throttle;

/**
 * Data to be transferred to the reality by a Reality Interface
 */
public class RealityOutputData {
	private final Throttle throttle;
	private final Elevator elevator;
	private final Aileron aileron;
	private final Rudder rudder;
	private final Flaps flaps; 
	private final Brake brake;

	public RealityOutputData(Throttle throttle, Elevator elevator, Aileron aileron, Rudder rudder, Flaps flaps, Brake brake) {
		this.throttle = throttle;
		this.elevator = elevator;
		this.aileron = aileron;
		this.rudder = rudder;
		this.flaps = flaps;
		this.brake = brake;
	}

	public Throttle getThrottle() {
		return throttle;
	}

	public Elevator getElevator() {
		return elevator;
	}

	public Aileron getAileron() {
		return aileron;
	}

	public Rudder getRudder() {
		return rudder;
	}
	
	public Flaps getFlaps()
	{
		return flaps; //TODO implement
	}
	
	public Brake getBrake()
	{
		return brake; //TODO implement
	}
}
