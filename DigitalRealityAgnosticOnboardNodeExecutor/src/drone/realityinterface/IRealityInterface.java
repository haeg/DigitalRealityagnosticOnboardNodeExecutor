/** IRealityInterface.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.realityinterface;

/**
 * Interface to be implemented by Reality Interfaces
 * The RealityInterface also contains the main-loop of the DRONE-System. So implementations will want to start a new Thread.
 */
public interface IRealityInterface {
	
	/**
	 * @return true if the Connection is running. During normal Operation this will not be false
	 */
	public boolean isConnected();
	
	/**
	 *  The DRONE system is ready for calls to executeCycle().
	 *  The Reality Interface should start its execution now.
	 */
	public void startExecution();
	
	/**
	 * The DRONE system can ask the Reality Interface to stop the execution when the Programm has end, so that the Application may stop
	 */
	public void stopExecution();
}
