/** PidController.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.util;

import drone.GeneralUtils;
import drone.logging.ILogChannel;

public class PidController {
	private double kP = 0.0;
	private double kI = 0.0;
	private double kD = 0.0;

	private double previousError = 0.0;
	private double integral = 0.0;
	private double lowerLimit = -Double.MAX_VALUE;
	private double upperLimit = Double.MAX_VALUE;

	private final ILogChannel logChannel;

	public PidController() {
		this(ILogChannel.NULL, 0.0, 0.0, 0.0);
	}

	public PidController(ILogChannel log) {
		this(log, 0.0, 0.0, 0.0);
	}

	public PidController(double kP, double kI, double kD) {
		this(ILogChannel.NULL, kP, kI, kD);
	}

	public PidController(ILogChannel log, double kP, double kI, double kD) {
		logChannel = log;
		this.kP = kP;
		this.kI = kI;
		this.kD = kD;
	}

	public double cycle(double error, double dT) {
		if (!GeneralUtils.equals(dT, 0.0, 0.000001)) {
			if (!GeneralUtils.equals(kI, 0.0, 0.000001)) {
				integral = GeneralUtils.limit((integral + error * dT), upperLimit / kI, lowerLimit / kI);
			}
			double derivative = (error - previousError) / dT;

			previousError = error;

			double outP = (kP * error);
			double outI = (kI * integral);
			double outD = (kD * derivative);
			double outSum = (outP + outI + outD);

			if (logChannel.isCyclicEnabled()) {
				logChannel.logCyclic(String.format("Limlo: %1$6.3f", lowerLimit));
				logChannel.logCyclic(String.format("Limup: %1$6.3f", upperLimit));
				logChannel.logCyclic(String.format("error: %1$6.3f", error));
				logChannel.logCyclic(String.format("integral: %1$6.3f", integral));
				logChannel.logCyclic(String.format("derivat: %1$6.3f", derivative));
				logChannel.logCyclic(String.format("outP: %1$6.3f", outP));
				logChannel.logCyclic(String.format("outI: %1$6.3f", outI));
				logChannel.logCyclic(String.format("outD: %1$6.3f", outD));
				logChannel.logCyclic(String.format("outSum: %1$6.3f", outSum));
				logChannel.logCyclic("---");
			}

			return GeneralUtils.limit(outSum, upperLimit, lowerLimit);
		} else {
			logChannel.logError("dT to small: " + dT);
			return 0.0;
		}
	}

	public PidController pid(double p, double i, double d) {
		this.kP = p;
		this.kI = i;
		this.kD = d;
		return this;
	}

	public PidController limitOutput(double lower, double upper) {
		lowerLimit = lower;
		upperLimit = upper;
		return this;
	}

	public PidController reset() {
		this.previousError = 0.0;
		this.integral = 0.0;
		return this;
	}

}
