/** GpsMath.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.util;

import drone.types.calc.Distance;
import drone.types.io.GpsPosition;
import drone.types.io.Heading;


/**
 * Math for working with GpsPositions.
 * porting from JavaScript to java, made by me. 
 * 
 * Original Algorithms:
 * 
 * (C) 2002-2016 Chris Veness  (MIT License)
 * scripts-geo@moveable-type.co.uk
 * http://www.movable-type.co.uk/scripts/latlong.html
 * 
 */
public class GpsMath {
	
	public static Distance distanceBetween(GpsPosition a, GpsPosition b)
	{
		double latAr = Math.toRadians(a.latitude());
		double latBr = Math.toRadians(b.latitude());
		double lonAr = Math.toRadians(a.longitude());
		double lonBr = Math.toRadians(b.longitude());
		double earthRadius = 6371000.0;
		
		double x = (lonBr - lonAr) * Math.cos((latAr + latBr)/2.0);
		double y = latBr - latAr;
		double dist = Math.sqrt(x*x + y*y) * earthRadius;
		
		return Distance.fromMeters(dist);
	}
	
	public static Heading HeadingFromTo(GpsPosition from, GpsPosition to)
	{
		double latAr = Math.toRadians(from.latitude());
		double latBr = Math.toRadians(to.latitude());
		double lonAr = Math.toRadians(from.longitude());
		double lonBr = Math.toRadians(to.longitude());
		
		double y = Math.sin(lonBr - lonAr) * Math.cos(latBr);
		double x = Math.cos(latAr)*Math.sin(latBr) - Math.sin(latAr)*Math.cos(latBr)*Math.cos(lonBr-lonAr);
		double brng = Math.toDegrees(Math.atan2(y, x));
		
		return Heading.fromDegrees(brng);
	}
	
	
	
	
	public static void main(String[] args)
	{
		GpsPosition point1 = GpsPosition.fromLatitideLongitude(53.568558, 8.788421);
		GpsPosition point2 = GpsPosition.fromLatitideLongitude(54.184739, 7.914864);
		
		System.out.println(String.format("dist: %1$1.3f", distanceBetween(point1, point2).kilometers()));
		System.out.println(String.format("head: %1$3.1f", HeadingFromTo(point1, point2).degrees()));
		
		
	}
}
