/** IDrone.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone;

import drone.control.plane.Controls;
import drone.data.IInputProperties;
import drone.data.IOutputProperties;
import drone.logging.ILogChannel;
import drone.persistence.IPersistence;

public interface IDrone {
	public IOutputProperties output();
	public IInputProperties input();
	public Controls control();
	public ILogChannel getLogChannel(String name);
	public IPersistence getPersistence();
	
	public String getStatusLine();
}
