/** IOutputProperty.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

public interface IOutputProperty<T> {
	public OutputMode mode();
	public void mode(OutputMode mode);
	public T fixed();
	public void fixed(T newValue);
	public T controlled();
	public void controlled(T newValue);
}
