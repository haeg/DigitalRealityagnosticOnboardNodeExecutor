/** IOutputProperties.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

import drone.types.io.Aileron;
import drone.types.io.Brake;
import drone.types.io.Elevator;
import drone.types.io.Flaps;
import drone.types.io.Rudder;
import drone.types.io.Throttle;

public interface IOutputProperties {
	public IOutputProperty<Throttle> throttle();
	
	public IOutputProperty<Aileron> aileron();

	public IOutputProperty<Elevator> elevator();

	public IOutputProperty<Rudder> rudder();
	
	public IOutputProperty<Flaps> flaps();
	
	public IOutputProperty<Brake> brake();
}
