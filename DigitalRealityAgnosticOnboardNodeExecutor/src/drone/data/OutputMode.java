/** OutputMode.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

public enum OutputMode {
	INPUT, FIXED, CONTROLLED
}
