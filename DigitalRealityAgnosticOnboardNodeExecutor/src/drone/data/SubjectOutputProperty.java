/** SubjectOutputProperty.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

import java.util.ArrayList;
import java.util.List;

import drone.gui.GenericComboBoxRow;
import drone.gui.GenericStringRow;
import drone.gui.SubjectTableFrame.Row;
import rx.subjects.BehaviorSubject;

public class SubjectOutputProperty<T> implements IOutputProperty<T>{
	private final String name;
	private final Class<T> valueClass;
	private final BehaviorSubject<T> fixedValueSubject;
	private final BehaviorSubject<T> controlledSubject;
	private final BehaviorSubject<OutputMode> modeSubject;

	public SubjectOutputProperty(String name, T defaultValue, Class<T> valueClass) {
		this.name = name;
		this.valueClass = valueClass;
		this.controlledSubject = BehaviorSubject.create(defaultValue);
		this.fixedValueSubject = BehaviorSubject.create(defaultValue);
		this.modeSubject = BehaviorSubject.create(OutputMode.FIXED);
	}
	
	public List<Row<?>> displayRows()
	{
		List<Row<?>> rows = new ArrayList<Row<?>>();
		rows.add(new GenericComboBoxRow<OutputMode>(name, modeSubject, OutputMode.values(), OutputMode.class));
		rows.add(new GenericStringRow<T>("fix", fixedValueSubject, true, valueClass));
		rows.add(new GenericStringRow<T>("controlled", controlledSubject, false, valueClass));
		return rows;
	}


	@Override
	public OutputMode mode() {
		return modeSubject.getValue();
	}

	@Override
	public void mode(OutputMode mode) {
		modeSubject.onNext(mode);
	}

	@Override
	public T fixed() {
		return fixedValueSubject.getValue();
	}

	@Override
	public void fixed(T newValue) {
		fixedValueSubject.onNext(newValue);
	}

	@Override
	public T controlled() {
		return controlledSubject.getValue();
	}

	@Override
	public void controlled(T newValue) {
		controlledSubject.onNext(newValue);
	}
}