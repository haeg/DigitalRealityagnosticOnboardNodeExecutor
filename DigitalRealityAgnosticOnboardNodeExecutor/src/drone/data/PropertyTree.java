/** PropertyTree.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

import java.util.ArrayList;
import java.util.List;

import drone.gui.SeparatorRow;
import drone.gui.SubjectTableFrame.Row;
import drone.realityinterface.IRealityInputData;
import drone.realityinterface.RealityOutputData;
import drone.types.calc.CycleTime;
import drone.types.calc.VerticalSpeed;
import drone.types.io.Altitude;

public class PropertyTree {
	private final SubjectInputProperties inputProperties;
	private final SubjectOutputProperties outputProperties;
	private final ValueCalculator calculator = new ValueCalculator();

	public PropertyTree() {
		inputProperties = new SubjectInputProperties();
		outputProperties = new SubjectOutputProperties();
	}

	public IInputProperties inputs() {
		return inputProperties;
	}

	public IOutputProperties outputs() {
		return outputProperties;
	}

	public void copyInputData(final IRealityInputData inputData, final CycleTime cycleTime) {
		inputProperties.cycleTime(cycleTime);

		inputProperties.throttle(inputData.getThrottle());
		inputProperties.elevator(inputData.getElevator());
		inputProperties.aileron(inputData.getAileron());
		inputProperties.rudder(inputData.getRudder());

		inputProperties.airSpeed(inputData.getAirSpeed());
		inputProperties.altitude(inputData.getAltitude());
		inputProperties.position(inputData.getPosition());

		inputProperties.pitch(inputData.getPitch());
		inputProperties.roll(inputData.getRoll());
		inputProperties.heading(inputData.getHeading());
	}
	
	public void calculateDerivedInputData()
	{
		inputProperties.verticalSpeed(calculator.calcVerticalSpeed());
		
		
		// TODO GroundSpeed (via GPS)
	}

	public RealityOutputData collectOutputData() {
		return new RealityOutputData(collectOutput(outputProperties.throttle(), inputProperties.throttle()),
				collectOutput(outputProperties.elevator(), inputProperties.elevator()),
				collectOutput(outputProperties.aileron(), inputProperties.aileron()),
				collectOutput(outputProperties.rudder(), inputProperties.rudder()),
				collectOutput(outputProperties.flaps(), inputProperties.flaps()),
				collectOutput(outputProperties.brake(), inputProperties.brake()));
	}

	private <T> T collectOutput(final IOutputProperty<T> output, final T input) {
		switch (output.mode()) {
		case FIXED:
			return output.fixed();
		case CONTROLLED:
			return output.controlled();

		default:
			return input;
		}
	}
	
	public List<Row<?>> displayRows() {
		List<Row<?>> rows = new ArrayList<Row<?>>();
		rows.addAll(inputProperties.displayRows());
		rows.add(new SeparatorRow("---- Outputs: ----"));
		rows.addAll(outputProperties.displayRows());
		return rows;
	}
	
	private class ValueCalculator
	{
		private double vSpeed = 0.0;
		private Altitude lastAltitude = Altitude.DEFAULT;
		
		public VerticalSpeed calcVerticalSpeed()
		{
			double vSpeedNew = (inputProperties.altitude().feet()-lastAltitude.feet()) / inputProperties.cycleTime().seconds() * 60.0;
			vSpeed = (vSpeed * 15.0) + vSpeedNew;
			vSpeed = vSpeed / 16.0;
			
			lastAltitude = inputProperties.altitude();
			return VerticalSpeed.fromFeetPerMinute(vSpeed);
		}
	}

}
