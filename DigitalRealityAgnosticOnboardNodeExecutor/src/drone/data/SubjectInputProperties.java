/** SubjectInputProperties.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

import java.util.ArrayList;
import java.util.List;

import drone.gui.GenericStringRow;
import drone.gui.SeparatorRow;
import drone.gui.SubjectTableFrame.Row;
import drone.types.calc.CycleTime;
import drone.types.calc.GroundSpeed;
import drone.types.calc.VerticalSpeed;
import drone.types.io.Aileron;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Brake;
import drone.types.io.Elevator;
import drone.types.io.Flaps;
import drone.types.io.Fuel;
import drone.types.io.GpsPosition;
import drone.types.io.Heading;
import drone.types.io.Pitch;
import drone.types.io.Roll;
import drone.types.io.Rudder;
import drone.types.io.Throttle;
import rx.subjects.BehaviorSubject;

public class SubjectInputProperties implements IInputProperties {
	private final BehaviorSubject<Throttle> throttle = BehaviorSubject.create(Throttle.DEFAULT);
	private final BehaviorSubject<Aileron> aileron = BehaviorSubject.create(Aileron.DEFAULT);
	private final BehaviorSubject<Elevator> elevator = BehaviorSubject.create(Elevator.DEFAULT);
	private final BehaviorSubject<Rudder> rudder = BehaviorSubject.create(Rudder.DEFAULT);
	private final BehaviorSubject<Flaps> flaps = BehaviorSubject.create(Flaps.DEFAULT);
	private final BehaviorSubject<Brake> brake = BehaviorSubject.create(Brake.DEFAULT);

	private final BehaviorSubject<AirSpeed> airSpeed = BehaviorSubject.create(AirSpeed.DEFAULT);
	private final BehaviorSubject<Altitude> altitude = BehaviorSubject.create(Altitude.DEFAULT);

	private final BehaviorSubject<GpsPosition> position = BehaviorSubject.create(GpsPosition.DEFAULT);

	private final BehaviorSubject<Heading> heading = BehaviorSubject.create(Heading.DEFAULT);
	private final BehaviorSubject<Pitch> pitch = BehaviorSubject.create(Pitch.DEFAULT);
	private final BehaviorSubject<Roll> roll = BehaviorSubject.create(Roll.DEFAULT);

	private final BehaviorSubject<CycleTime> cycleTime = BehaviorSubject.create(CycleTime.DEFAULT);
	private final BehaviorSubject<GroundSpeed> groundSpeed = BehaviorSubject.create(GroundSpeed.DEFAULT);
	private final BehaviorSubject<VerticalSpeed> verticalSpeed = BehaviorSubject.create(VerticalSpeed.DEFAULT);
	
	private final BehaviorSubject<Fuel> fuel = BehaviorSubject.create(Fuel.DEFAULT);

	@Override
	public Throttle throttle() {
		return throttle.getValue();
	}

	@Override
	public Aileron aileron() {
		return aileron.getValue();
	}

	@Override
	public Elevator elevator() {
		return elevator.getValue();
	}

	@Override
	public Rudder rudder() {
		return rudder.getValue();
	}
	
	@Override
	public Flaps flaps() {
		return flaps.getValue();
	}
	
	@Override
	public Brake brake() {
		return brake.getValue();
	}

	@Override
	public AirSpeed airSpeed() {
		return airSpeed.getValue();
	}

	@Override
	public Altitude altitude() {
		return altitude.getValue();
	}

	@Override
	public GpsPosition position() {
		return position.getValue();
	}

	@Override
	public Heading heading() {
		return heading.getValue();
	}

	@Override
	public Pitch pitch() {
		return pitch.getValue();
	}

	@Override
	public Roll roll() {
		return roll.getValue();
	}

	@Override
	public CycleTime cycleTime() {
		return cycleTime.getValue();
	}

	@Override
	public VerticalSpeed verticalSpeed() {
		return verticalSpeed.getValue();
	}

	@Override
	public GroundSpeed groundSpeed() {
		return groundSpeed.getValue();
	}
	@Override
	public Fuel fuel() {
		return fuel.getValue();
	}

	public void throttle(Throttle newValue) {
		throttle.onNext(newValue);
	}

	public void aileron(Aileron newValue) {
		aileron.onNext(newValue);
	}

	public void elevator(Elevator newValue) {
		elevator.onNext(newValue);
	}

	public void rudder(Rudder newValue) {
		rudder.onNext(newValue);
	}
	
	public void flaps(Flaps newValue)
	{
		flaps.onNext(newValue);
	}
	
	public void brake(Brake newValue)
	{
		brake.onNext(newValue);
	}

	public void airSpeed(AirSpeed newValue) {
		airSpeed.onNext(newValue);
	}

	public void altitude(Altitude newValue) {
		altitude.onNext(newValue);
	}

	public void position(GpsPosition newValue) {
		position.onNext(newValue);
	}

	public void heading(Heading newValue) {
		heading.onNext(newValue);
	}

	public void pitch(Pitch newValue) {
		pitch.onNext(newValue);
	}

	public void roll(Roll newValue) {
		roll.onNext(newValue);
	}

	public void cycleTime(CycleTime newValue) {
		cycleTime.onNext(newValue);
	}

	public void verticalSpeed(VerticalSpeed newValue) {
		verticalSpeed.onNext(newValue);
	}

	public void groundSpeed(GroundSpeed newValue) {
		groundSpeed.onNext(newValue);
	}
	
	public void fuel(Fuel newValue)
	{
		fuel.onNext(newValue);
	}



	public List<Row<?>> displayRows() {
		List<Row<?>> rows = new ArrayList<Row<?>>();
		rows.add(new SeparatorRow("---- Inputs ----"));
		rows.add(new GenericStringRow<AirSpeed>("Speed (air)", airSpeed, false, AirSpeed.class));
		rows.add(new GenericStringRow<Altitude>("Altitude", altitude, false, Altitude.class));
		rows.add(new GenericStringRow<GpsPosition>("GPS position", position, false, GpsPosition.class));
		rows.add(new GenericStringRow<Pitch>("Pitch", pitch, true, Pitch.class));
		rows.add(new GenericStringRow<Roll>("Roll", roll, true, Roll.class));
		rows.add(new GenericStringRow<Heading>("Heading", heading, false, Heading.class));
		rows.add(new GenericStringRow<Fuel>("Fuel", fuel, false, Fuel.class));
		rows.add(new SeparatorRow("---- Calculated: ----"));
		rows.add(new GenericStringRow<CycleTime>("cycleTime", cycleTime, false, CycleTime.class));
		rows.add(new GenericStringRow<GroundSpeed>("Speed (ground)", groundSpeed, false, GroundSpeed.class));
		rows.add(new GenericStringRow<VerticalSpeed>("Speed (vertical)", verticalSpeed, false, VerticalSpeed.class));

		return rows;
	}



}
