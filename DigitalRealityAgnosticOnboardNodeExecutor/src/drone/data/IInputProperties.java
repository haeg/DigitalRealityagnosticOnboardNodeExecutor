/** IInputProperties.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

import drone.types.calc.CycleTime;
import drone.types.calc.GroundSpeed;
import drone.types.calc.VerticalSpeed;
import drone.types.io.Aileron;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Brake;
import drone.types.io.Elevator;
import drone.types.io.Flaps;
import drone.types.io.Fuel;
import drone.types.io.GpsPosition;
import drone.types.io.Heading;
import drone.types.io.Pitch;
import drone.types.io.Roll;
import drone.types.io.Rudder;
import drone.types.io.Throttle;

public interface IInputProperties {
	
	public Throttle throttle();
	public Aileron aileron();
	public Elevator elevator();
	public Rudder rudder();
	public Flaps flaps();
	public Brake brake();
	
	public AirSpeed airSpeed();
	public Altitude altitude();
	public GpsPosition position();
	
	public Heading heading();
	public Pitch pitch();
	public Roll roll();
	
	public CycleTime cycleTime();
	public VerticalSpeed verticalSpeed();
	public GroundSpeed groundSpeed();	
	
	public Fuel fuel();
	

}
