/** SubjectOutputProperties.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.data;

import java.util.ArrayList;
import java.util.List;

import drone.gui.SubjectTableFrame.Row;
import drone.types.io.Aileron;
import drone.types.io.Brake;
import drone.types.io.Elevator;
import drone.types.io.Flaps;
import drone.types.io.Rudder;
import drone.types.io.Throttle;

public class SubjectOutputProperties implements IOutputProperties{
	private final SubjectOutputProperty<Throttle> throttle = new SubjectOutputProperty<Throttle>("Throttle", Throttle.DEFAULT, Throttle.class);
	private final SubjectOutputProperty<Aileron> aileron = new SubjectOutputProperty<Aileron>("Aileron", Aileron.DEFAULT, Aileron.class);
	private final SubjectOutputProperty<Elevator> elevator = new SubjectOutputProperty<Elevator>("Elevator", Elevator.DEFAULT, Elevator.class);
	private final SubjectOutputProperty<Rudder> rudder = new SubjectOutputProperty<Rudder>("Rudder", Rudder.DEFAULT, Rudder.class);
	private final SubjectOutputProperty<Flaps> flaps = new SubjectOutputProperty<Flaps>("Flaps", Flaps.DEFAULT, Flaps.class);
	private final SubjectOutputProperty<Brake> brake = new SubjectOutputProperty<Brake>("Brake", Brake.DEFAULT, Brake.class);
	@Override
	public IOutputProperty<Throttle> throttle()
	{
		return throttle;
	}
	@Override
	public IOutputProperty<Aileron> aileron()
	{
		return aileron;
	}
	@Override
	public IOutputProperty<Elevator> elevator()
	{
		return elevator;
	}
	@Override
	public IOutputProperty<Rudder> rudder()
	{
		return rudder;
	}
	
	@Override
	public IOutputProperty<Flaps> flaps() {
		return flaps;
	}
	
	@Override
	public IOutputProperty<Brake> brake() {
		return brake;
	}
	
	public List<Row<?>> displayRows()
	{
		List<Row<?>> rows = new ArrayList<Row<?>>();
		rows.addAll(throttle.displayRows());
		rows.addAll(aileron.displayRows());
		rows.addAll(elevator.displayRows());
		rows.addAll(rudder.displayRows());
		rows.addAll(flaps.displayRows());
		rows.addAll(brake.displayRows());
		return rows;
	}

}
