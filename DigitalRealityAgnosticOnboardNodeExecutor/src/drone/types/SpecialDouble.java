/** SpecialDouble.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types;

public abstract class SpecialDouble {
	protected final Double value;

	protected SpecialDouble() {
		value = 0.0;
	}

	protected SpecialDouble(String s) {
		this(Double.valueOf(s));
	}

	protected SpecialDouble(Double d) {
		value = Math.min(maxValue(), Math.max(d, minValue()));
	}

	@Override
	public String toString() {
		return String.format(formatString(), value);
	}

	protected String formatString() {
		return "%1$6.1f";
	}

	abstract protected Double minValue();

	abstract protected Double maxValue();
}
