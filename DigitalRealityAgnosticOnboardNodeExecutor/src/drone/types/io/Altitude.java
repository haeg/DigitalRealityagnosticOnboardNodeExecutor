/** Altitude.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

public class Altitude extends SpecialDouble {
	public static final Altitude DEFAULT = new Altitude(0.0);

	public static Altitude fromFeet(double feet) {
		return new Altitude(feet);
	}

	public double feet() {
		return value;
	}

	@Deprecated
	public Altitude(String s) {
		super(s);
	}

	protected Altitude(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -Double.MAX_VALUE;
	}

	@Override
	protected Double maxValue() {
		return Double.MAX_VALUE;
	}

}
