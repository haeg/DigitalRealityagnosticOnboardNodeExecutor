/** Aileron.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * positive numbers make the plane go clockwise
 */
public class Aileron extends SpecialDouble {
	public static final Aileron DEFAULT = new Aileron(0.0);

	public static Aileron fromPercent(double percent) {
		return new Aileron(percent);
	}

	public double percent() {
		return value;
	}

	@Deprecated
	public Aileron(String s) {
		super(s);
	}

	protected Aileron(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -100.0;
	}

	@Override
	protected Double maxValue() {
		return 100.0;
	}

}
