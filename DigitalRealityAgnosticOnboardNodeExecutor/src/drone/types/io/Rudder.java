/** Rudder.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * positive numbers make the nose go right
 */
public class Rudder extends SpecialDouble {
	public static final Rudder DEFAULT = new Rudder(0.0);

	public static Rudder fromPercent(double percent) {
		return new Rudder(percent);
	}

	public double percent() {
		return value;
	}

	@Deprecated
	public Rudder(String s) {
		super(s);
	}

	protected Rudder(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -100.0;
	}

	@Override
	protected Double maxValue() {
		return 100.0;
	}

}
