/** Heading.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

public class Heading extends SpecialDouble {
	public static final Heading DEFAULT = new Heading(0.0);

	public static Heading fromDegrees(double degrees) {
		return new Heading(degrees);
	}

	public Heading inverted() {
		return new Heading(degrees() - 180.0);
	}

	public double degrees() {
		return value;
	}

	public double errorTo(Heading other) {
		double error = other.degrees() - this.degrees();
		if (error > 180.0) {
			return error - 360.0;
		}
		if (error < -180.0) {
			return error + 360.0;
		}
		return error;
	}

	protected Heading(Double d) {
		super(limit(d));
	}

	@Deprecated
	public Heading(String s) {
		this(Double.parseDouble(s));
	}

	@Override
	protected Double minValue() {
		return 0.0;
	}

	@Override
	protected Double maxValue() {
		return 359.999999;
	}

	private static double limit(double angle) {
		while (angle < 0.0) {
			angle += 360.0;
		}
		while (angle > 360.0) {
			angle -= 360.0;
		}
		return angle;
	}
}
