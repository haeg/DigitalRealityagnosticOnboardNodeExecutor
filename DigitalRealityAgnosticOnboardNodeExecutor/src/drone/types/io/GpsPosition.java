/** GpsPosition.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

/**
 * Gps Coordinated Latitude: N/S (N = +) Longitude: E/W (E = +)
 *
 */
public class GpsPosition {
	private final Double latitude;
	private final Double longitude;

	public static final GpsPosition DEFAULT = new GpsPosition(0.0, 0.0);

	public static GpsPosition fromLatitideLongitude(double lat, double lon) {
		return new GpsPosition(lat, lon);
	}

	public double latitude() {
		return latitude;
	}

	public double longitude() {
		return longitude;
	}

	protected GpsPosition(Double latitude, Double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	/**
	 * format: "Double/Double"
	 * 
	 * @param s
	 * @deprecated
	 */
	public GpsPosition(String s) {
		this(getLat(s), getLong(s));
	}

	@Override
	public String toString() {
		return String.format("%1$6.6f/%2$6.6f", latitude, longitude);
	}

	private static Double getLat(String s) {
		return Double.parseDouble(s.split("/")[0]);
	}

	private static Double getLong(String s) {
		return Double.parseDouble(s.split("/")[1]);
	}

}
