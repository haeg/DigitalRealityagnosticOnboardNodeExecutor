/** Fuel.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * amount of fuel in percent
 *
 */
public class Fuel extends SpecialDouble {
	public static final Fuel DEFAULT = new Fuel(0.0);

	public static Fuel fromPercent(double percent) {
		return new Fuel(percent);
	}

	public double percent() {
		return value;
	}

	@Deprecated
	public Fuel(String s) {
		super(s);
	}

	protected Fuel(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return 0.0;
	}

	@Override
	protected Double maxValue() {
		return 100.0;
	}

}
