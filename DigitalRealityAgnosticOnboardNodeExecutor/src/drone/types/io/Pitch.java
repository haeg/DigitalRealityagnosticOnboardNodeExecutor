/** Pitch.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * PitchAngle; + is up
 */
public class Pitch extends SpecialDouble {
	public static final Pitch DEFAULT = new Pitch(0.0);

	public static Pitch fromDegrees(double degrees) {
		return new Pitch(degrees);
	}

	public double degrees() {
		return value;
	}

	@Deprecated
	public Pitch(String s) {
		super(s);
	}

	protected Pitch(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -90.0;
	}

	@Override
	protected Double maxValue() {
		return 90.0;
	}

}
