/** AirSpeed.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

public class AirSpeed extends SpecialDouble {
	public static final AirSpeed DEFAULT = new AirSpeed(0.0);

	public static AirSpeed fromKnots(double knots) {
		return new AirSpeed(knots);
	}
	
	public static AirSpeed fromMetersPerSecond(double mps)
	{
		return new AirSpeed(mps/0.5144);
	}
	
	public static AirSpeed fromKilometersPerHour(double kph)
	{
		return new AirSpeed(kph/1.852);
	}

	public double knots() {
		return value;
	}
	
	public double metersPerSecond()
	{
		return value*0.5144;
	}
	
	public double kilometersPerHour()
	{
		return value*1.852;
	}

	@Deprecated
	public AirSpeed(String s) {
		super(s);
	}

	protected AirSpeed(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -Double.MAX_VALUE;
	}

	@Override
	protected Double maxValue() {
		return Double.MAX_VALUE;
	}

}
