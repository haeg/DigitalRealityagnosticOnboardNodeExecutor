/** Flaps.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * flaps in percent
 *
 */
public class Flaps extends SpecialDouble {
	public static final Flaps DEFAULT = new Flaps(0.0);

	public static Flaps fromPercent(double percent) {
		return new Flaps(percent);
	}

	public double percent() {
		return value;
	}

	@Deprecated
	public Flaps(String s) {
		super(s);
	}

	protected Flaps(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return 0.0;
	}

	@Override
	protected Double maxValue() {
		return 100.0;
	}

}
