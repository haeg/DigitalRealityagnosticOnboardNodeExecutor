/** Brake.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * brake power in percent
 *
 */
public class Brake extends SpecialDouble {
	public static final Brake DEFAULT = new Brake(0.0);

	public static Brake fromPercent(double percent) {
		return new Brake(percent);
	}

	public double percent() {
		return value;
	}

	@Deprecated
	public Brake(String s) {
		super(s);
	}

	protected Brake(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return 0.0;
	}

	@Override
	protected Double maxValue() {
		return 100.0;
	}

}
