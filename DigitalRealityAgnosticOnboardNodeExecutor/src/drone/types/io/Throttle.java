/** Throttle.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * power of the engine
 *
 */
public class Throttle extends SpecialDouble {
	public static final Throttle DEFAULT = new Throttle(0.0);

	public static Throttle fromPercent(double percent) {
		return new Throttle(percent);
	}

	public double percent() {
		return value;
	}

	@Deprecated
	public Throttle(String s) {
		super(s);
	}

	protected Throttle(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return 0.0;
	}

	@Override
	protected Double maxValue() {
		return 100.0;
	}

}
