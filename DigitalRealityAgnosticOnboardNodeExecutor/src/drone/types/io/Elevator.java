/** Elevator.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * positive numbers make the nose go up
 */
public class Elevator extends SpecialDouble {
	public static final Elevator DEFAULT = new Elevator(0.0);

	public static Elevator fromPercent(double percent) {
		return new Elevator(percent);
	}

	public double percent() {
		return value;
	}

	@Deprecated
	public Elevator(String s) {
		super(s);
	}

	protected Elevator(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -100.0;
	}

	@Override
	protected Double maxValue() {
		return 100.0;
	}

}
