/** Roll.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.io;

import drone.types.SpecialDouble;

/**
 * RollAngle; + is clockwise
 */
public class Roll extends SpecialDouble {
	public static final Roll DEFAULT = new Roll(0.0);
	public static Roll fromDegrees(double degrees)
	{
		return new Roll(degrees);
	}
	public double degrees()
	{
		return value;
	}

	@Deprecated
	public Roll(String s) {
		super(s);
	}

	protected Roll(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -180.0;
	}

	@Override
	protected Double maxValue() {
		return 180.0;
	}


}
