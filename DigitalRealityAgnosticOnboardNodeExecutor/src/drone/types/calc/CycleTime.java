/** CycleTime.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.calc;

import drone.types.SpecialDouble;

public class CycleTime extends SpecialDouble {
	public static final CycleTime DEFAULT = new CycleTime(0.0);

	public static CycleTime fromSeconds(double seconds) {
		return new CycleTime(seconds);
	}

	public double seconds() {
		return value;
	}

	@Deprecated
	public CycleTime(String s) {
		super(s);
	}

	protected CycleTime(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return 0.0;
	}

	@Override
	protected Double maxValue() {
		return Double.MAX_VALUE;
	}

	@Override
	protected String formatString() {
		return "%1$6.3f";
	}
}
