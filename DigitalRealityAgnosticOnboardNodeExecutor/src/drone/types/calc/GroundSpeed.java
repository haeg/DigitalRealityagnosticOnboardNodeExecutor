/** GroundSpeed.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.calc;

import drone.types.SpecialDouble;

public class GroundSpeed extends SpecialDouble {
	public static final GroundSpeed DEFAULT = new GroundSpeed(0.0);

	public static GroundSpeed fromKnots(double knots) {
		return new GroundSpeed(knots);
	}

	public double knots() {
		return value;
	}

	@Deprecated
	public GroundSpeed(String s) {
		super(s);
	}

	protected GroundSpeed(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -Double.MAX_VALUE;
	}

	@Override
	protected Double maxValue() {
		return Double.MAX_VALUE;
	}
}
