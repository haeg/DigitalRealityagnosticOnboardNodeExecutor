/** Distance.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.calc;

import drone.types.SpecialDouble;

public class Distance extends SpecialDouble {
	public static final Distance DEFAULT = new Distance(0.0);

	public static Distance fromMeters(double meters) {
		return new Distance(meters);
	}
	
	public static Distance fromFeet(double feet)
	{
		return new Distance(feet/3.28084);
	}
	
	public static Distance fromKilometers(double kilometers)
	{
		return new Distance(kilometers*1000.0);
	}
	
	public static Distance fromMiles(double miles)
	{
		return Distance.fromKilometers(miles*1.609344);
	}
	
	public static Distance fromNauticalMiles(double nmiles)
	{
		return Distance.fromKilometers(nmiles*1.852);
	}

	public double meters() {
		return value;
	}

	public double kilometers() {
		return value/1000.0;
	}
	
	public double miles()
	{
		return kilometers()/1.609344;
	}
	
	public double nauticalMiles()
	{
		return kilometers()/1.852;
	}
	
	public double feet()
	{
		return value*3.28084;
	}

	@Deprecated
	public Distance(String s) {
		super(s);
	}

	protected Distance(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return 0.0;
	}

	@Override
	protected Double maxValue() {
		return Double.MAX_VALUE;
	}

	@Override
	protected String formatString() {
		return "%1$6.3f";
	}
}
