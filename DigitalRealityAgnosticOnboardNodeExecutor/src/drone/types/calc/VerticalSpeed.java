/** VerticalSpeed.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.types.calc;

import drone.types.SpecialDouble;

/**
 * VerticalSpeed in feet per minute
 */
public class VerticalSpeed extends SpecialDouble {
	public static final VerticalSpeed DEFAULT = new VerticalSpeed(0.0);

	public static final VerticalSpeed fromFeetPerMinute(double fpm) {
		return new VerticalSpeed(fpm);
	}

	public double feetPerMinute() {
		return value;
	}

	@Deprecated
	public VerticalSpeed(String s) {
		super(s);
	}

	protected VerticalSpeed(Double d) {
		super(d);
	}

	@Override
	protected Double minValue() {
		return -Double.MAX_VALUE;
	}

	@Override
	protected Double maxValue() {
		return Double.MAX_VALUE;
	}
}
