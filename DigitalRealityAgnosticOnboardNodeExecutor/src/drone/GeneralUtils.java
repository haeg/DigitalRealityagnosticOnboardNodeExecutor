/** GeneralUtils.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone;

import java.lang.reflect.Constructor;

public class GeneralUtils {

	public static double limit(double value, double upper, double lower)
	{
		return Math.min(upper, Math.max(value, lower));
	}
	
	public static boolean equals(double a, double b, double accuracy)
	{
		return (Math.abs(a-b) < accuracy);
	}
	
	public static <T> T createFromString(String string, Class<T> typeClass) {
		try {
			Constructor<T> stringConstructor = typeClass.getConstructor(String.class);
			return stringConstructor.newInstance(string);
		} catch (Exception e) {
			throw new IllegalArgumentException(string, e);
		}
	}


}
