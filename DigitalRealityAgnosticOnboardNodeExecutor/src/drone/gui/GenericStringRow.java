/** GenericStringRow.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.gui;

import javax.swing.table.TableCellEditor;

import drone.GeneralUtils;
import drone.gui.SubjectTableFrame.Row;
import rx.subjects.BehaviorSubject;

public class GenericStringRow<T> implements Row<T>{
	private final String name;
	private final BehaviorSubject<T> subject;
	private final Class<T> clazz;
	private final boolean editable;
	
	public GenericStringRow(String name, BehaviorSubject<T> subject, boolean editable, Class<T> clazz) {
		this.name = name;
		this.subject = subject;
		this.editable = editable;
		this.clazz = clazz;
	}

	@Override
	public String getName() {
		return (editable ? "[rw] " : "[r] ") + name;
	}
	
	@Override
	public BehaviorSubject<T> getSubject() {
		return subject;
	}
	
	@Override
	public boolean isEditable() {
		return editable;
	}
	@Override
	public TableCellEditor getEditor() {
		return null;
	}
	@Override
	public String getTypeName() {
		return clazz.getSimpleName();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T convertInput(Object editorInput) throws IllegalArgumentException{
		
		if(clazz.isInstance(editorInput))
		{
			return (T)editorInput;
		}
		else if(editorInput instanceof String)
		{
			return GeneralUtils.createFromString((String) editorInput, clazz);

		}
		else
		{
			throw new IllegalArgumentException(editorInput != null ? editorInput.toString() : "null");
		}
	}
}
