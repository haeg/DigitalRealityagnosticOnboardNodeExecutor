/** SubjectTableFrame.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;

import rx.Subscription;
import rx.subjects.BehaviorSubject;

@SuppressWarnings("serial")
public class SubjectTableFrame extends JFrame {
	private final SubjectTable table;
	private final SubjectTableModel tableModel;
	
	public SubjectTableFrame(final String title, final List<SubjectTableFrame.Row<?>> rows) {
		super(title);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		tableModel = new SubjectTableModel(rows);
		table = new SubjectTable(tableModel);
		JScrollPane scroller = new JScrollPane(table);
		this.add(scroller);
		pack();
	}

	@Override
	public void dispose() {
		tableModel.dispose();
		super.dispose();
	}

	public interface Row<T> {
		public String getName();

		public BehaviorSubject<T> getSubject();

		public boolean isEditable();

		public TableCellEditor getEditor();

		public String getTypeName();

		public T convertInput(Object editorInput) throws IllegalArgumentException;
	}

	private static class SubjectTable extends JTable {
		private final List<SubjectTableFrame.Row<?>> rows;

		public SubjectTable(final SubjectTableModel model) {
			super(model);

			rows = model.rows;
		}

		@Override
		public TableCellEditor getCellEditor(int row, int column) {
			TableCellEditor editor = rows.get(row).getEditor();

			if (editor == null) {
				editor = super.getCellEditor(row, column);
			}

			return editor;
		}
	}

	public static class SubjectTableModel extends AbstractTableModel {
		private final List<SubjectTableFrame.Row<?>> rows;
		private final List<Subscription> subscriptionList = new ArrayList<Subscription>();

		public SubjectTableModel(final List<SubjectTableFrame.Row<?>> rows) {
			super();
			this.rows = rows;

			rows.forEach(r -> subscriptionList.add(r.getSubject().distinctUntilChanged().subscribe(t -> fireTableDataChanged())));

		}

		public void dispose() {
			subscriptionList.forEach(s -> s.unsubscribe());
		}

		@Override
		public int getColumnCount() {
			return 3;
		}

		@Override
		public String getColumnName(int columnIndex) {
			final String returnValue;
			switch (columnIndex) {
			case 0:
				returnValue = "Name";
				break;
			case 1:
				returnValue = "Value";
				break;
			case 2:
				returnValue = "Type";
				break;

			default:
				returnValue = "INVALID";
			}

			return returnValue;
		}

		@Override
		public int getRowCount() {
			return rows.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			final Object returnValue;
			switch (columnIndex) {
			case 0:
				returnValue = rows.get(rowIndex).getName();
				break;
			case 1:
				returnValue = rows.get(rowIndex).getSubject().getValue();
				break;
			case 2:
				returnValue = rows.get(rowIndex).getTypeName();
				break;
			default:
				returnValue = "INVALID";
			}

			return returnValue;
		}

		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return (columnIndex == 1) && (rows.get(rowIndex).isEditable());
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			try {				
				Row row = rows.get(rowIndex);
				Object value = row.convertInput(aValue);
				if (value != null) {
					row.getSubject().onNext(value);
				}
			} catch (IllegalArgumentException e) {
				//just do no update
			}
		}
	}

}
