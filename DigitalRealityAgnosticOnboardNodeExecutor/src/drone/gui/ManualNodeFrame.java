/** ManualNodeFrame.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import drone.nodes.ManualNode;
import drone.types.calc.VerticalSpeed;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Heading;
import drone.types.io.Pitch;
import drone.types.io.Roll;

/**
 * created using WindowBuilder
 */
public class ManualNodeFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTextField headingTargetField;
	private JTextField altitudetargetField;
	private JTextField speedTargetField;
	private JTextField rollTargetField;
	private JTextField pitchTargetField;
	private JTextField verticalTargetField;
	public ManualNodeFrame(ManualNode node) {
		super("ManualControl");
		getContentPane().setLayout(null);
		
		JPanel airSpeed = new JPanel();
		airSpeed.setBorder(new TitledBorder(null, "AirSpeed Hold", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		airSpeed.setBounds(10, 11, 239, 106);
		getContentPane().add(airSpeed);
		airSpeed.setLayout(null);
		
		JSlider airspeedTargetSlider = new JSlider();
		speedTargetField = new JTextField();
		
		airspeedTargetSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				speedTargetField.setText(String.valueOf(airspeedTargetSlider.getValue()));
				node.speedHoldTarget = AirSpeed.fromKnots(airspeedTargetSlider.getValue());
			}
		});
		airspeedTargetSlider.setMajorTickSpacing(50);
		airspeedTargetSlider.setMaximum(120);
		airspeedTargetSlider.setPaintLabels(true);
		airspeedTargetSlider.setPaintTicks(true);
		airspeedTargetSlider.setMinorTickSpacing(10);
		airspeedTargetSlider.setValue(0);
		airspeedTargetSlider.setBounds(10, 45, 221, 54);
		airSpeed.add(airspeedTargetSlider);
		
		JCheckBox airspeedActive = new JCheckBox("active");
		airspeedActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				node.speedHoldEnabled = airspeedActive.isSelected();
			}
		});
		airspeedActive.setBounds(10, 15, 97, 23);
		airSpeed.add(airspeedActive);
		
		
		speedTargetField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				airspeedTargetSlider.setValue(Integer.parseInt(speedTargetField.getText()));
			}
		});
		speedTargetField.setText("0");
		speedTargetField.setBounds(113, 16, 86, 20);
		airSpeed.add(speedTargetField);
		speedTargetField.setColumns(10);
		
		JPanel roll = new JPanel();
		roll.setLayout(null);
		roll.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Roll Hold", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		roll.setBounds(10, 128, 239, 106);
		getContentPane().add(roll);
		
		JSlider rollTargetSlider = new JSlider();
		rollTargetField = new JTextField();
		rollTargetSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				rollTargetField.setText(String.valueOf(rollTargetSlider.getValue()));
				node.rollHoldTarget = Roll.fromDegrees(rollTargetSlider.getValue());
			}
		});
		rollTargetSlider.setMinimum(-50);
		rollTargetSlider.setValue(0);
		rollTargetSlider.setPaintTicks(true);
		rollTargetSlider.setPaintLabels(true);
		rollTargetSlider.setMinorTickSpacing(10);
		rollTargetSlider.setMaximum(50);
		rollTargetSlider.setMajorTickSpacing(50);
		rollTargetSlider.setBounds(10, 45, 221, 54);
		roll.add(rollTargetSlider);
		
		JCheckBox rollActive = new JCheckBox("active");
		rollActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				node.rollHoldEnabled = rollActive.isSelected();
			}
		});
		rollActive.setBounds(10, 15, 97, 23);
		roll.add(rollActive);
		
		
		rollTargetField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rollTargetSlider.setValue(Integer.parseInt(rollTargetField.getText()));
			}
		});
		rollTargetField.setText("0");
		rollTargetField.setBounds(113, 16, 86, 20);
		roll.add(rollTargetField);
		rollTargetField.setColumns(10);
		
		JPanel heading = new JPanel();
		heading.setLayout(null);
		heading.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Heading Hold", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		heading.setBounds(259, 128, 239, 106);
		getContentPane().add(heading);
		
		headingTargetField = new JTextField();
		headingTargetField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				node.headingHoldTarget = Heading.fromDegrees(Double.parseDouble(headingTargetField.getText()));
			}
		});
		headingTargetField.setText("359");
		headingTargetField.setBounds(9, 75, 86, 20);
		heading.add(headingTargetField);
		headingTargetField.setColumns(10);
		
		JCheckBox headingActive = new JCheckBox("active");
		headingActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				node.headingHoldEnabled = headingActive.isSelected();
			}
		});
		headingActive.setBounds(9, 15, 97, 23);
		heading.add(headingActive);
		
		JPanel pitch = new JPanel();
		pitch.setLayout(null);
		pitch.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Pitch Hold", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pitch.setBounds(10, 245, 239, 106);
		getContentPane().add(pitch);
		
		JSlider pitchTargetSlider = new JSlider();
		pitchTargetField = new JTextField();
		pitchTargetSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				pitchTargetField.setText(String.valueOf(pitchTargetSlider.getValue()));
				node.pitchHoldTarget = Pitch.fromDegrees(pitchTargetSlider.getValue());
			}
		});
		pitchTargetSlider.setValue(0);
		pitchTargetSlider.setPaintTicks(true);
		pitchTargetSlider.setPaintLabels(true);
		pitchTargetSlider.setMinorTickSpacing(10);
		pitchTargetSlider.setMinimum(-30);
		pitchTargetSlider.setMaximum(30);
		pitchTargetSlider.setMajorTickSpacing(30);
		pitchTargetSlider.setBounds(10, 45, 221, 54);
		pitch.add(pitchTargetSlider);
		
		JCheckBox pitchActive = new JCheckBox("active");
		pitchActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				node.pitchHoldEnabled = pitchActive.isSelected();
			}
		});
		pitchActive.setBounds(10, 15, 97, 23);
		pitch.add(pitchActive);
		
		
		pitchTargetField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pitchTargetSlider.setValue(Integer.parseInt(pitchTargetField.getText()));
			}
		});
		pitchTargetField.setText("0");
		pitchTargetField.setBounds(113, 16, 86, 20);
		pitch.add(pitchTargetField);
		pitchTargetField.setColumns(10);
		
		JPanel vertical = new JPanel();
		vertical.setLayout(null);
		vertical.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Vertical Speed Hold", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		vertical.setBounds(259, 245, 239, 106);
		getContentPane().add(vertical);
		
		JSlider verticalTargetSlider = new JSlider();
		verticalTargetField = new JTextField();
		verticalTargetSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				verticalTargetField.setText(String.valueOf(verticalTargetSlider.getValue()));
				node.verticalHoldTarget = VerticalSpeed.fromFeetPerMinute(verticalTargetSlider.getValue());
			}
		});
		verticalTargetSlider.setValue(0);
		verticalTargetSlider.setPaintTicks(true);
		verticalTargetSlider.setPaintLabels(true);
		verticalTargetSlider.setMinorTickSpacing(100);
		verticalTargetSlider.setMinimum(-1000);
		verticalTargetSlider.setMaximum(1000);
		verticalTargetSlider.setMajorTickSpacing(500);
		verticalTargetSlider.setBounds(10, 45, 221, 54);
		vertical.add(verticalTargetSlider);
		
		JCheckBox verticalActive = new JCheckBox("active");
		verticalActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				node.verticalHoldEnabled = verticalActive.isSelected();
			}
		});
		verticalActive.setBounds(10, 15, 97, 23);
		vertical.add(verticalActive);
		
		
		verticalTargetField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verticalTargetSlider.setValue(Integer.parseInt(verticalTargetField.getText()));
			}
		});
		verticalTargetField.setText("0");
		verticalTargetField.setBounds(113, 16, 86, 20);
		vertical.add(verticalTargetField);
		verticalTargetField.setColumns(10);
		
		JPanel altitude = new JPanel();
		altitude.setLayout(null);
		altitude.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Altitude Hold", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		altitude.setBounds(508, 245, 239, 106);
		getContentPane().add(altitude);
		
		altitudetargetField = new JTextField();
		altitudetargetField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				node.altitudeHoldTarget = Altitude.fromFeet(Double.parseDouble(altitudetargetField.getText()));
			}
		});
		altitudetargetField.setText("1000");
		altitudetargetField.setBounds(10, 75, 86, 20);
		altitude.add(altitudetargetField);
		altitudetargetField.setColumns(10);
		
		JCheckBox altitudeActive = new JCheckBox("active");
		altitudeActive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				node.altitudeHoldEnabled = altitudeActive.isSelected();
			}
		});
		altitudeActive.setBounds(10, 12, 97, 23);
		altitude.add(altitudeActive);
		setSize(775, 400);
		
		
	}
}
