/** GenericComboBoxRow.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.gui;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.TableCellEditor;

import drone.gui.SubjectTableFrame.Row;
import rx.subjects.BehaviorSubject;

public class GenericComboBoxRow<T> implements Row<T> {
	private final String name;
	private final BehaviorSubject<T> subject;
	private final Class<T> clazz;
	private final TableCellEditor editor;
	
	public GenericComboBoxRow(String name, BehaviorSubject<T> subject, T[] values, Class<T> clazz) {
		this.name = name;
		this.subject = subject;
		this.clazz = clazz;
		
		editor = new DefaultCellEditor(new JComboBox<T>(values));
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public BehaviorSubject<T> getSubject() {
		return subject;
	}

	@Override
	public boolean isEditable() {
		return true;
	}

	@Override
	public TableCellEditor getEditor() {
		return editor;
	}

	
	@Override
	public String getTypeName() {
		return clazz.getSimpleName();
	}

	@SuppressWarnings("unchecked")
	@Override
	public T convertInput(Object editorInput) throws IllegalArgumentException{
		if(clazz.isInstance(editorInput))
		{
			return (T)editorInput;
		}
		else
		{
			throw new IllegalArgumentException(editorInput != null ? editorInput.toString() : "null");
		}
	}
}
