/** SeparatorRow.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.gui;

import javax.swing.table.TableCellEditor;

import drone.gui.SubjectTableFrame.Row;
import rx.subjects.BehaviorSubject;

public class SeparatorRow implements Row<String> {
	private final String name;
	private final BehaviorSubject<String> dummy = BehaviorSubject.create("");
	
	public SeparatorRow(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public BehaviorSubject<String> getSubject() {
		return dummy;
	}

	@Override
	public boolean isEditable() {
		return false;
	}

	@Override
	public TableCellEditor getEditor() {
		return null;
	}

	@Override
	public String getTypeName() {
		return "--";
	}

	@Override
	public String convertInput(Object editorInput) throws IllegalArgumentException {
		return null;
	}

}
