/** ILogChannelImpl.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.logging;

/**
 *
 */
public class ILogChannelImpl implements ILogChannel {
	private final LogChannelConfig config;
	private final LogChannelProvider provider;
	private final boolean cyclicEnabled;
	private final boolean infoEnabled;
	private final boolean warnEnabled;
	private final boolean errorEnabled;

	public ILogChannelImpl(final LogChannelConfig config, LogChannelProvider provider)
	{
		this.config = config;
		this.provider = provider;
		cyclicEnabled = (config.getConsoleLevel() >= ILogChannel.CYCLIC) || (config.getNetworkLevel() >= ILogChannel.CYCLIC)
				|| (config.getDatabaseLevel() >= ILogChannel.CYCLIC);
		infoEnabled = (config.getConsoleLevel() >= ILogChannel.INFO) || (config.getNetworkLevel() >= ILogChannel.INFO)
				|| (config.getDatabaseLevel() >= ILogChannel.INFO);
		warnEnabled = (config.getConsoleLevel() >= ILogChannel.WARNING) || (config.getNetworkLevel() >= ILogChannel.WARNING)
				|| (config.getDatabaseLevel() >= ILogChannel.WARNING);
		errorEnabled = (config.getConsoleLevel() >= ILogChannel.ERROR) || (config.getNetworkLevel() >= ILogChannel.ERROR)
				|| (config.getDatabaseLevel() >= ILogChannel.ERROR);
	}

	public LogChannelConfig getConfig()
	{
		return config;
	}

	@Override
	public void logCyclic(String msg) {
		if(cyclicEnabled)
		{
			provider.log(this, CYCLIC, msg);
		}
	}

	@Override
	public boolean isCyclicEnabled() {
		return cyclicEnabled;
	}

	@Override
	public void logInfo(String msg) {
		if(infoEnabled)
		{
			provider.log(this, INFO, msg);
		}
	}

	@Override
	public boolean isInfoEnabled() {
		return infoEnabled;
	}

	@Override
	public void logWarning(String msg) {
		if(warnEnabled)
		{
			provider.log(this, WARNING, msg);
		}
	}

	@Override
	public boolean isWarningEnabled() {
		return warnEnabled;
	}

	@Override
	public void logError(String msg) {
		if(errorEnabled)
		{
			provider.log(this, ERROR, msg);
		}
	}

	@Override
	public boolean isErrorEnabled() {
		return errorEnabled;
	}

}
