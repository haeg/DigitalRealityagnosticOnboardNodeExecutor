/** LogChannelProvider.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import drone.IDrone;

public class LogChannelProvider {
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
	private final Map<String, ILogChannelImpl> logChannels;
	
	public LogChannelProvider(IDrone drone) {
		logChannels = new HashMap<String, ILogChannelImpl>();
		for(LogChannelConfig config : drone.getPersistence().getAll("logchannel", LogChannelConfig.class))
		{
			logChannels.put(config.getName(), new ILogChannelImpl(config, this));
		}
	}
	
	public ILogChannel getLogChannel(String name)
	{
		if(logChannels.containsKey(name))
		{
			return logChannels.get(name);
		}
		else
		{
			logChannels.get("logchannel").logWarning(String.format("logchannel %s not found", name));
			return new ILogChannelImpl(new LogChannelConfig(name, ILogChannel.WARNING, ILogChannel.OFF, ILogChannel.OFF), this);
		}
	}

	public void log(ILogChannelImpl logChannel, int level, String message)
	{
		if(logChannel.getConfig().getConsoleLevel() >= level)
		{
			logConsole(logChannel.getConfig().getName(), level, message);
		}
		
		if(logChannel.getConfig().getNetworkLevel() >= level)
		{
			logNetwork(logChannel.getConfig().getName(), level, message);
		}
		
		if(logChannel.getConfig().getDatabaseLevel() >= level)
		{
			logDatabase(logChannel.getConfig().getName(), level, message);
		}
	}
	
	
	private void logConsole(String name, int level, String message)
	{
		switch (level) {
		case ILogChannel.CYCLIC:
			System.out.println(String.format("%s cyc\t%s: %s", dateFormat.format(new Date()), name, message));
			break;
		case ILogChannel.INFO:
			System.out.println(String.format("%s info\t%s: %s", dateFormat.format(new Date()), name, message));
			break;
		case ILogChannel.WARNING:
			System.out.println(String.format("%s WARN\t%s: %s", dateFormat.format(new Date()), name, message));
			break;
		case ILogChannel.ERROR:
			System.out.println(String.format("%s ERROR ###\t%s: %s", dateFormat.format(new Date()), name, message));
			break;
		}
	}
	
	private void logNetwork(String name, int level, String message)
	{
		//TODO implement network logging 
		//udp -> try to safe bandwith, do only transmit name as index, list of names on request, levels as byte, only text as string
	}
	
	private void logDatabase(String name, int level, String message)
	{
		//TODO implement database logging
		//database -> no idea until now
	}
}
