/** ILogChannel.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.logging;

public interface ILogChannel {
	public static final int OFF = 0;
	public static final int ERROR = 1;
	public static final int WARNING = 2;
	public static final int INFO = 3;
	public static final int CYCLIC = 4;
	
	public void logCyclic(String msg);
	public boolean isCyclicEnabled();
	public void logInfo(String msg);
	public boolean isInfoEnabled();
	public void logWarning(String msg);
	public boolean isWarningEnabled();
	public void logError(String msg);
	public boolean isErrorEnabled();
	
	public static ILogChannel NULL = new ILogChannel() {
		@Override
		public void logWarning(String msg) {}
		@Override
		public void logInfo(String msg) {}
		@Override
		public void logError(String msg) {}
		@Override
		public void logCyclic(String msg) {}
		@Override
		public boolean isCyclicEnabled() {
			return false;
		}
		@Override
		public boolean isInfoEnabled() {
			return false;
		}
		@Override
		public boolean isWarningEnabled() {
			return false;
		}
		@Override
		public boolean isErrorEnabled() {
			return false;
		}
	};
}
