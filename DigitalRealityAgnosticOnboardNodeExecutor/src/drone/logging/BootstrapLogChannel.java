/** BootstrapLogChannel.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.logging;

/**
 * Use only while actual LogProvider is not yet available
 */
public class BootstrapLogChannel implements ILogChannel {
	private final int logLevel;
	private final String name;

	public BootstrapLogChannel(final String name, final int logLevel) {
		this.name = name;
		this.logLevel = logLevel;
	}

	@Override
	public void logCyclic(String msg) {
		if(isCyclicEnabled())
		{
			System.out.println(String.format("--:--:--.--- cyc\t%s: %s", name, msg));
		}
	}

	@Override
	public void logInfo(String msg) {
		if(isInfoEnabled())
		{
			System.out.println(String.format("--:--:--.--- info\t%s: %s", name, msg));
		}
	}

	@Override
	public void logWarning(String msg) {
		if(isWarningEnabled())
		{
			System.out.println(String.format("--:--:--.--- WARN\t%s: %s", name, msg));
		}
	}

	@Override
	public void logError(String msg) {
		if(isErrorEnabled())
		{
			System.out.println(String.format("--:--:--.--- ERROR ###\t%s: %s", name, msg));
		}
	}	
	
	@Override
	public boolean isCyclicEnabled() {
		return logLevel >= CYCLIC;
	}

	@Override
	public boolean isInfoEnabled() {
		return logLevel >= INFO;
	}

	@Override
	public boolean isWarningEnabled() {
		return logLevel >= WARNING;
	}

	@Override
	public boolean isErrorEnabled() {
		return logLevel >= ERROR;
	}

}
