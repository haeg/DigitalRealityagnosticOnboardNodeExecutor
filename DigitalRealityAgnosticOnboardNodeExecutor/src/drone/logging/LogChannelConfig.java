/** LogChannelConfig.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.logging;

/**
 *
 */
public class LogChannelConfig {
	private final String name;
	private final int consoleLevel;
	private final int networkLevel;
	private final int databaseLevel;
	
	public LogChannelConfig(final String name, final int consoleLevel, final int networkLevel, final int databaseLevel)
	{
		this.name = name;
		this.consoleLevel = consoleLevel;
		this.networkLevel = networkLevel;
		this.databaseLevel = databaseLevel;
	}
	
	public LogChannelConfig(final String persistenceLine) {
		String[] splited = persistenceLine.split(",");
		name = splited[0];
		consoleLevel = Integer.parseInt(splited[1]);
		networkLevel = Integer.parseInt(splited[2]);
		databaseLevel = Integer.parseInt(splited[3]);
	}

	public String getName() {
		return name;
	}

	public int getConsoleLevel() {
		return consoleLevel;
	}

	public int getNetworkLevel() {
		return networkLevel;
	}

	public int getDatabaseLevel() {
		return databaseLevel;
	}

}
