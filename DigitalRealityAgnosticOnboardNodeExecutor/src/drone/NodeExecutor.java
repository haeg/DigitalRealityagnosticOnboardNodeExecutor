/** NodeExecutor.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone;

import drone.logging.ILogChannel;
import drone.nodes.INode;
import drone.nodes.INodeProvider;
import rx.subjects.BehaviorSubject;

public class NodeExecutor {
	private final INodeProvider nodeProvider;
	private final ILogChannel logChannel;
	private INode currentNode;
	
	private final BehaviorSubject<String> currentNodeName = BehaviorSubject.create("");
	private final BehaviorSubject<String> currentNodeStatus = BehaviorSubject.create("");
	
	public NodeExecutor(INodeProvider nodeProvider, IDrone drone) {
		this.nodeProvider = nodeProvider;
		this.logChannel = drone.getLogChannel("node.executor");
		currentNode = nodeProvider.getNode();
	}
	
	public void executeStep()
	{
		if(currentNode.isFinished())
		{
			logChannel.logInfo("has finished: " + currentNode.getName());
			currentNode.deactivate();
			currentNode = nodeProvider.getNode();
			currentNode.activate();
			currentNodeName.onNext(currentNode.getName());
			logChannel.logInfo("activated: " + currentNode.getName());
		}
		
		currentNode.execute();
		currentNodeStatus.onNext(currentNode.getStatusLine());
		logChannel.logCyclic(currentNode.getStatusLine());
	}
	
	public String getStatusLine()
	{
		return currentNode != null ? currentNode.getStatusLine() : "";
	}
}
