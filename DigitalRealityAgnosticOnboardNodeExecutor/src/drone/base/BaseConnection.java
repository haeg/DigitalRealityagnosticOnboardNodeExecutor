/** BaseConnection.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.base;

import drone.IDrone;
import drone.types.calc.CycleTime;

/**
 *
 */
public class BaseConnection {
	private final IDrone drone;
	public BaseConnection(IDrone drone) {
		this.drone = drone;
	}
	
	public void cycle(CycleTime time)
	{
		//TODO check time, if rate reached collect data and statusLine, send packet to BASE
	}
	
	public void connect(){
		//TODO
	}
}
