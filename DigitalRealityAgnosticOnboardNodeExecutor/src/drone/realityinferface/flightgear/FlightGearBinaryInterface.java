/** FlightGearBinaryInterface.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.realityinferface.flightgear;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import drone.realityinterface.IDroneRealityInterface;
import drone.realityinterface.IRealityInterface;
import drone.realityinterface.RealityOutputData;
import drone.types.calc.CycleTime;

public class FlightGearBinaryInterface implements IRealityInterface {
	private static final String LOGCHANNEL_NAME = "realityinterface";

	private final IDroneRealityInterface drone;

	private final int listenPort;
	private final int sendPort;
	private final String sendHost;
	private final int receiveLength;
	private final int sendLength;

	private DatagramSocket datagramSocket;
	private final ByteBuffer byteBuffer;
	private long lastMillis = 0;

	private boolean connectionOK = false;

	private final Thread worker = new Thread() {
		public void run() {
			drone.getLogChannel(LOGCHANNEL_NAME).logInfo("workerThread started... ");
			InetAddress adress = null;
			try {
				datagramSocket = new DatagramSocket(listenPort);
				adress = InetAddress.getByName(sendHost);
			} catch (SocketException | UnknownHostException e) {
				drone.getLogChannel(LOGCHANNEL_NAME).logError("while setting up socket: " + e.getMessage());
				return;
			}
			connectionOK = true;
			drone.getLogChannel(LOGCHANNEL_NAME).logInfo(String.format("listening on port: %d", listenPort));

			while (datagramSocket != null) {
				DatagramPacket rcvPacket = new DatagramPacket(byteBuffer.array(), byteBuffer.capacity());
				try {
					datagramSocket.receive(rcvPacket);
				} catch (IOException e) {
					drone.getLogChannel(LOGCHANNEL_NAME).logError("while receiving: " + e.getMessage());
					connectionOK = false;
					return;
				}

				final long currentMillis = System.currentTimeMillis();
				CycleTime cycleTime = CycleTime.fromSeconds((currentMillis - lastMillis) / 1000.0);
				lastMillis = currentMillis;
				drone.getLogChannel(LOGCHANNEL_NAME)
						.logCyclic(String.format("recieved packet, cycletime: %1$4.3f seconds", cycleTime.seconds()));

				RealityOutputData outputData = drone.executeCycle(new FlightGearInputData(byteBuffer), cycleTime);
				fillOutputBuffer(outputData);

				final DatagramPacket sendPacket = new DatagramPacket(byteBuffer.array(), sendLength, adress, sendPort);

				try {
					datagramSocket.send(sendPacket);
				} catch (IOException e) {
					drone.getLogChannel(LOGCHANNEL_NAME).logError("while sending: " + e.getMessage());
					connectionOK = false;
					return;
				}

				
			}
			drone.getLogChannel(LOGCHANNEL_NAME).logInfo("workerThread ended... ");
		}
	};

	public FlightGearBinaryInterface(final IDroneRealityInterface drone) {
		this.drone = drone;
		listenPort = drone.getPersistence().getSingle("FlightGearBinaryInterfaceWorker.listenPort", 0, Integer.class);
		sendPort = drone.getPersistence().getSingle("FlightGearBinaryInterfaceWorker.sendPort", 0, Integer.class);
		sendHost = drone.getPersistence().getSingle("FlightGearBinaryInterfaceWorker.sendHost", "", String.class);
		receiveLength = drone.getPersistence().getSingle("FlightGearBinaryInterfaceWorker.receiveLength", 0,
				Integer.class);
		sendLength = drone.getPersistence().getSingle("FlightGearBinaryInterfaceWorker.sendLength", 0, Integer.class);

		this.byteBuffer = ByteBuffer.allocate(receiveLength);
		if (listenPort == 0 || sendPort == 0 || sendHost.isEmpty() || receiveLength == 0 || sendLength == 0) {
			drone.getLogChannel(LOGCHANNEL_NAME)
					.logError(String.format(
							"default Values read from Persistence: listenPort=%d, sendPort=%d, sendHost=%s, receiveLength=%d, sendLength=%d",
							listenPort, sendPort, sendHost, receiveLength, sendLength));
		}

	}

	@Override
	public void startExecution() {
		drone.getLogChannel(LOGCHANNEL_NAME).logInfo("FlightGearBinaryInterface#startExecution() start worker");
		worker.start();
	}

	@Override
	public void stopExecution() {
		drone.getLogChannel(LOGCHANNEL_NAME).logInfo("FlightGearBinaryInterface#stopExecution() disconnect worker");
		datagramSocket.close();
		datagramSocket = null;
	}

	@Override
	public boolean isConnected() {
		return connectionOK;
	}

	private void fillOutputBuffer(RealityOutputData data) {
		byteBuffer.putFloat(0, (float) (data.getThrottle().percent() / 100.0));
		byteBuffer.putFloat(4, (float) (data.getElevator().percent() / -100.0));
		byteBuffer.putFloat(8, (float) (data.getAileron().percent() / 100.0));
		byteBuffer.putFloat(12, (float) (data.getRudder().percent() / 100.0));
	}

}
