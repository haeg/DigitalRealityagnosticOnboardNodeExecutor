/** FlightGearCyclicInputData.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone.realityinferface.flightgear;

import java.nio.ByteBuffer;

import drone.realityinterface.IRealityInputData;

import drone.types.io.Aileron;
import drone.types.io.AirSpeed;
import drone.types.io.Altitude;
import drone.types.io.Elevator;
import drone.types.io.Fuel;
import drone.types.io.GpsPosition;
import drone.types.io.Heading;
import drone.types.io.Pitch;
import drone.types.io.Roll;
import drone.types.io.Rudder;
import drone.types.io.Throttle;

public class FlightGearInputData implements IRealityInputData {
	private final Throttle throttle;
	private final Elevator elevator;
	private final Aileron aileron;
	private final Rudder rudder;
	private final AirSpeed airSpeed;
	private final Altitude altitude;
	private final GpsPosition position;
	private final Pitch pitch;
	private final Roll roll;
	private final Heading heading;
	private final Fuel fuel;

	public FlightGearInputData(ByteBuffer buffer) {
		throttle = Throttle.fromPercent((double) buffer.getFloat(0) * 100.0f);
		elevator = Elevator.fromPercent((double) buffer.getFloat(4) * -100.0f);
		aileron = Aileron.fromPercent((double) buffer.getFloat(8) * 100.0f);
		rudder = Rudder.fromPercent((double) buffer.getFloat(12) * 100.0f);
		airSpeed = AirSpeed.fromKnots((double) buffer.getFloat(16));
		altitude = Altitude.fromFeet((double) buffer.getFloat(20));
		position = GpsPosition.fromLatitideLongitude((double) buffer.getFloat(24), 
				(double) buffer.getFloat(28));
		pitch = Pitch.fromDegrees((double) buffer.getFloat(32));
		roll = Roll.fromDegrees((double) buffer.getFloat(36));
		heading = Heading.fromDegrees((double) buffer.getFloat(40));
		fuel = Fuel.DEFAULT;//TODO add fuel to flightgear interface
	}

	@Override
	public Throttle getThrottle() {
		return throttle;
	}

	@Override
	public Elevator getElevator() {
		return elevator;
	}

	@Override
	public Aileron getAileron() {
		return aileron;
	}

	@Override
	public Rudder getRudder() {
		return rudder;
	}

	@Override
	public AirSpeed getAirSpeed() {
		return airSpeed;
	}

	@Override
	public Altitude getAltitude() {
		return altitude;
	}

	@Override
	public GpsPosition getPosition() {
		return position;
	}

	@Override
	public Pitch getPitch() {
		return pitch;
	}

	@Override
	public Roll getRoll() {
		return roll;
	}

	@Override
	public Heading getHeading() {
		return heading;
	}

	@Override
	public Fuel getFuel() {
		return fuel;
	}
}
