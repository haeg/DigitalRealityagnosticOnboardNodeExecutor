/** FlightgearRealityInterface.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.realityinterfaces;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import drone2.interfaces.IInputSource;
import drone2.interfaces.IOuputSink;

public class FlightgearRealityInterface implements IInputSource, IOuputSink {
	private final int RECEIVE_PORT = 5555;
	private final int RECEIVE_LENGTH = 32;
	private final int SEND_PORT = 5556;
	private final String SEND_HOST = "localhost";
	private final int SEND_LENGTH = 16;

	private final int OUT_INDEX_THROTTLE = 0;
	private final int OUT_INDEX_ELEVATOR = 4;
	private final int OUT_INDEX_AILERON = 8;
	private final int OUT_INDEX_RUDDER = 12;
	private final double OUT_FACTOR_THROTTLE = 100.0;
	private final double OUT_FACTOR_ELEVATOR = -100.0;
	private final double OUT_FACTOR_AILERON = 100.0;
	private final double OUT_FACTOR_RUDDER = 100.0;

	private final int IN_INDEX_PITCH = 0;
	private final int IN_INDEX_ROLL = 4;
	private final int IN_INDEX_HEADING = 8;
	private final int IN_INDEX_POS_LAT = 12;
	private final int IN_INDEX_POS_LON = 16;
	private final int IN_INDEX_ALTITUDE = 20;
	private final int IN_INDEX_AIRSPEED = 24;
	private final int IN_INDEX_FUEL = 28;
	private final double IN_FACTOR_PITCH = 1.0;
	private final double IN_FACTOR_ROLL = 1.0;
	private final double IN_FACTOR_HEADING = 1.0;
	private final double IN_FACTOR_POS_LAT = 1.0;
	private final double IN_FACTOR_POS_LON = 1.0;
	private final double IN_FACTOR_ALTITUDE = 1.0;
	private final double IN_FACTOR_AIRSPEED = 1.0;
	private final double IN_FACTOR_FUEL = 100.0;

	private DatagramSocket datagramSocket;
	private final ByteBuffer inputBuffer = ByteBuffer.allocate(RECEIVE_LENGTH);
	private final ByteBuffer outputBuffer = ByteBuffer.allocate(SEND_LENGTH);

	private final Object mutex = new Object();

	private final Thread worker = new Thread() {
		public void run() {
			InetAddress adress = null;
			try {
				datagramSocket = new DatagramSocket(RECEIVE_PORT);
				adress = InetAddress.getByName(SEND_HOST);
			} catch (SocketException | UnknownHostException e) {
				System.err.println("while setting up socket: " + e.getMessage());
				return;
			}

			while (datagramSocket != null) {
				DatagramPacket rcvPacket = new DatagramPacket(inputBuffer.array(), inputBuffer.capacity());
				try {
					datagramSocket.receive(rcvPacket);
				} catch (IOException e) {
					System.err.println("while receiving: " + e.getMessage());
					return;
				}

				// printInput();

				synchronized (mutex) {
					final DatagramPacket sendPacket = new DatagramPacket(outputBuffer.array(), SEND_LENGTH, adress,
							SEND_PORT);

					try {
						datagramSocket.send(sendPacket);
					} catch (IOException e) {
						System.err.println("while sending: " + e.getMessage());
						return;
					}
				}

			}
		};
	};

	public FlightgearRealityInterface() {
		worker.start();
	}

	public void shutdown() {
		datagramSocket.close();
		datagramSocket = null;
	}

	private void printInput() {
		System.out.println("----");
		System.out.println(String.format("%1s \t: %2$1.4f", "Pitch", getPitch()));
		System.out.println(String.format("%1s \t: %2$1.4f", "Roll", getRoll()));
		System.out.println(String.format("%1s \t: %2$1.4f", "Heading", getHeading()));
		System.out.println(String.format("%1s \t: %2$1.4f", "Lati", getPositionLatitude()));
		System.out.println(String.format("%1s \t: %2$1.4f", "Long", getPositionLongituide()));
		System.out.println(String.format("%1s \t: %2$1.4f", "Altitude", getAltitude()));
		System.out.println(String.format("%1s \t: %2$1.4f", "AirSpeed", getAirSpeed()));
		System.out.println(String.format("%1s \t: %2$1.4f", "Fuel", getFuel()));
		System.out.println("----");
	}

	@Override
	public void setElevator(double elevator) {
		synchronized (mutex) {
			outputBuffer.putFloat(OUT_INDEX_ELEVATOR, (float) (elevator / OUT_FACTOR_ELEVATOR));
		}

	}

	@Override
	public void setAileron(double aileron) {
		synchronized (mutex) {
			outputBuffer.putFloat(OUT_INDEX_AILERON, (float) (aileron / OUT_FACTOR_AILERON));
		}

	}

	@Override
	public void setRudder(double rudder) {
		synchronized (mutex) {
			outputBuffer.putFloat(OUT_INDEX_RUDDER, (float) (rudder / OUT_FACTOR_RUDDER));
		}

	}

	@Override
	public void setThrottle(double throttle) {
		synchronized (mutex) {
			outputBuffer.putFloat(OUT_INDEX_THROTTLE, (float) (throttle / OUT_FACTOR_THROTTLE));
		}

	}

	@Override
	public double getPitch() {
		return inputBuffer.getFloat(IN_INDEX_PITCH) * IN_FACTOR_PITCH;
	}

	@Override
	public double getRoll() {
		return inputBuffer.getFloat(IN_INDEX_ROLL) * IN_FACTOR_ROLL;
	}

	@Override
	public double getHeading() {
		return inputBuffer.getFloat(IN_INDEX_HEADING) * IN_FACTOR_HEADING;
	}

	@Override
	public double getPositionLatitude() {
		return inputBuffer.getFloat(IN_INDEX_POS_LAT) * IN_FACTOR_POS_LAT;
	}

	@Override
	public double getPositionLongituide() {
		return inputBuffer.getFloat(IN_INDEX_POS_LON) * IN_FACTOR_POS_LON;
	}

	@Override
	public double getAltitude() {
		return inputBuffer.getFloat(IN_INDEX_ALTITUDE) * IN_FACTOR_ALTITUDE;
	}

	@Override
	public double getAirSpeed() {
		return inputBuffer.getFloat(IN_INDEX_AIRSPEED) * IN_FACTOR_AIRSPEED;
	}

	@Override
	public double getFuel() {
		return inputBuffer.getFloat(IN_INDEX_FUEL) * IN_FACTOR_FUEL;
	}

	public static void main(String[] args) {
		System.out.println("starting");
		new FlightgearRealityInterface();
	}
}
