/** HeadingMath.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.math;

public class HeadingMath {
	/**
	 * @return difference between a and b in degrees [0 .. +180]
	 */
	public static double difference(double a, double b)
	{
		double difference = a - b;
		if (difference > 180.0) {
			return difference - 360.0;
		}
		if (difference < -180.0) {
			return difference + 360.0;
		}
		return difference;
	}
	
	/**
	 * @return inverted heading
	 */
	public static double invert(double heading)
	{
		return limit(heading -180.0);
	}
	
	/**
	 * @return limits the heading to a valid range [0 .. +360]
	 */
	public static double limit(double heading)
	{
		while (heading < 0.0) {
			heading += 360.0;
		}
		while (heading > 360.0) {
			heading -= 360.0;
		}
		return heading;
	}
}
