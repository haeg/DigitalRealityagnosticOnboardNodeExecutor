/** IOuputSink.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.interfaces;

public interface IOuputSink {

	/**
	 * negative -> make nose go down
	 * @param elevator to be sent in percent [-100 .. +100]
	 */
	public void setElevator(double elevator);
	
	/**
	 * negative -> make plane go counter clockwise
	 * @param aileron to be sent in percent [-100 .. +100]
	 */
	public void setAileron(double aileron);
	
	/**
	 * negative -> make nose go left
	 * @param rudder to be sent in percent [-100 .. +100]
	 */
	public void setRudder(double rudder);
	
	/**
	 * @param throttle to be sent in percent [0 .. +100]
	 */
	public void setThrottle(double throttle);
}
