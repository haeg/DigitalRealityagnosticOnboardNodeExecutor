/** ICalcSource.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.interfaces;

public interface ICalcSource {
	/**
	 * @return Vertical Speed in feet per minute [-inf .. +inf]
	 */
	public double getVerticalSpeed();
	
	/**
	 * @return Ground Speed in meter per second [0 .. +inf]
	 */
	public double getGroundSpeed();
	
	/**
	 * @return Heading of the GroundSpeed in degrees [0 .. +360]
	 */
	public double getGroundSpeedHeading();
	
	/**
	 * negative -> going backwards
	 * @return front/rear component of the current GroundSpeed in meters per second [-inf .. +inf]
	 */
	public double GroundSpeedFrontRear();
	
	/**
	 * negative -> going left
	 * @return right/left component of the current GroundSpeed in meters per second [-inf .. +inf]
	 */
	public double GroundSpeedRightLeft();
}
