/** IInputSource.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.interfaces;

/**
 * 
 * @author Hannes Eggert
 *
 */
public interface IInputSource {
	/**
	 * negative -> nose down
	 * @return Pitch in degrees [-90 .. +90]
	 */
	public double getPitch();
	
	/**
	 * negative -> left down
	 * @return Roll in degrees [-180 .. +180]
	 */
	public double getRoll();
	
	/**
	 * @return Heading in degrees [0 .. +360]
	 */
	public double getHeading();
	
	/**
	 * @return Latitude (N/S) of current Position in degrees [-90 .. +90]
	 */
	public double getPositionLatitude();
	
	/**
	 * return Longitude (E/W) of current Position in degrees [-180 .. +180]
	 */
	public double getPositionLongituide();
	
	/**
	 * @return Altitude in feet [-inf .. +inf]
	 */
	public double getAltitude();
	
	/**
	 * @return AirSpeed in knots [0 .. +inf]
	 */
	public double getAirSpeed();
	
	/**
	 * @return Fuel in percent [0 .. +100]
	 */
	public double getFuel();
}
