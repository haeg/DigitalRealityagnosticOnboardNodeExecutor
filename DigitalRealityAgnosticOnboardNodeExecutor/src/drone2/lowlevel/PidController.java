/** PidController.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.lowlevel;


public class PidController {
	private double kP = 0.0;
	private double kI = 0.0;
	private double kD = 0.0;

	private double previousError = 0.0;
	private double integral = 0.0;
	private double lowerLimit = -Double.MAX_VALUE;
	private double upperLimit = Double.MAX_VALUE;

	public PidController() {
		this(0.0, 0.0, 0.0);
	}
	
	public PidController(double lower, double upper)
	{
		this(0.0,0.0,0.0);
		this.limitOutput(lower, upper);
	}


	public PidController(double kP, double kI, double kD) {
		this.kP = kP;
		this.kI = kI;
		this.kD = kD;
	}
	
	public double cycle(double error, int cycle)
	{
		double dT = cycle / 1000.0;
		
		integral = integral + (error * dT);
		integral = Math.max(integral, lowerLimit);
		integral = Math.min(integral, upperLimit);
		
		double derivative = (error - previousError) / dT;
		
		previousError = error;
		
		double outP = (kP * error);
		double outI = (kI * integral);
		double outD = (kD * derivative);
		double outSum = (outP + outI + outD);
		
		outSum = Math.min(outSum, upperLimit);
		outSum = Math.max(outSum, lowerLimit);
		return outSum;
	}

	public PidController pid(double p, double i, double d) {
		this.kP = p;
		this.kI = i;
		this.kD = d;
		return this;
	}

	public PidController limitOutput(double lower, double upper) {
		lowerLimit = lower;
		upperLimit = upper;
		return this;
	}

	public PidController reset() {
		this.previousError = 0.0;
		this.integral = 0.0;
		return this;
	}

}
