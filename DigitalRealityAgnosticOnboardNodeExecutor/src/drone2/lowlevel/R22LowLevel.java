/** R22LowLevel.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.lowlevel;

import drone2.gui.R22Frame;
import drone2.interfaces.IInputSource;
import drone2.interfaces.IOuputSink;
import drone2.math.HeadingMath;
import drone2.realityinterfaces.FlightgearRealityInterface;

public class R22LowLevel {
	private final IInputSource inputSouce;
	private final IOuputSink outputSink;

	private enum ConfigValues {
		ELEVATOR_P, ELEVATOR_I, ELEVATOR_D, AILERON_P, AILERON_I, AILERON_D, RUDDER_P, RUDDER_I, RUDDER_D,
	}

	private double getValue(ConfigValues value) {
		switch (value) {
		case ELEVATOR_P: return 1.0;
		case ELEVATOR_I: return 0.5;
		case ELEVATOR_D: return 0.2;
			
		case AILERON_P: return 1.0;
		case AILERON_I: return 0.5;
		case AILERON_D: return 0.2;
			
		case RUDDER_P: return 1.0;
		case RUDDER_I: return 0.5;
		case RUDDER_D: return 0.2;

		default:
			return 0.0;
		}
	}

	private final int CYCLE = 100;//cycleTime in ms

	private double targetPitch = 0.0;
	private final PidController elevatorPid = new PidController(-100.0, 100.0);
	private double targetRoll = 0.0;
	private final PidController aileronPid = new PidController(-100.0, 100.0);
	private double targetHeading = 0.0;
	private final PidController rudderPid = new PidController(-100.0, 100.0);

	private Thread worker = new Thread() {
		public void run() {
			while (!isInterrupted()) {
				
				elevatorPid.pid(getValue(ConfigValues.ELEVATOR_P), getValue(ConfigValues.ELEVATOR_I),
						getValue(ConfigValues.ELEVATOR_D));
				double pitchError = targetPitch - inputSouce.getPitch();
				double elevator = elevatorPid.cycle(pitchError, CYCLE);
				outputSink.setElevator(elevator);
				
				aileronPid.pid(getValue(ConfigValues.AILERON_P), getValue(ConfigValues.AILERON_I),
						getValue(ConfigValues.AILERON_D));
				double rollError = targetRoll - inputSouce.getRoll();
				double aileron = aileronPid.cycle(rollError, CYCLE);
				outputSink.setAileron(aileron);
				
				rudderPid.pid(getValue(ConfigValues.RUDDER_P), getValue(ConfigValues.RUDDER_I),
						getValue(ConfigValues.RUDDER_D));
				double headingError = HeadingMath.difference(targetHeading, inputSouce.getHeading());
				double rudder = rudderPid.cycle(headingError, CYCLE);
				outputSink.setRudder(rudder);
				
				
				System.out.println(String.format("%1s\t: %2$1.2f", "Elev" , elevator));
				System.out.println(String.format("%1s\t: %2$1.2f", "Aile" , aileron));
				System.out.println(String.format("%1s\t: %2$1.2f", "Rudd" , rudder));
				

				try {
					Thread.sleep(CYCLE);
				} catch (InterruptedException e) {
					interrupt();
				}
			}
		};
	};

	public R22LowLevel(IInputSource inputSource, IOuputSink outputSink) {
		this.inputSouce = inputSource;
		this.outputSink = outputSink;
		outputSink.setThrottle(100.0);
		worker.start();
	}

	public void shutdown() {
		if (worker != null) {
			worker.interrupt();
			worker = null;
		}
	}

	public void setTargetPitch(double targetPitch) {
		System.err.println(targetPitch);
		this.targetPitch = targetPitch;
	}

	public void setTargetRoll(double targetRoll) {
		System.err.println(targetRoll);
		this.targetRoll = targetRoll;
	}

	public void setTargetHeading(double targetHeading) {
		System.err.println(targetHeading);
		this.targetHeading = targetHeading;
	}

	public void setThrottle(double throttle) {
		System.err.println(throttle);
		outputSink.setThrottle(throttle);
	}

	public static void main(String[] args) {
		FlightgearRealityInterface ri = new FlightgearRealityInterface();
		R22LowLevel ll = new R22LowLevel(ri, ri);
		
		new R22Frame(ll);
	}
}
