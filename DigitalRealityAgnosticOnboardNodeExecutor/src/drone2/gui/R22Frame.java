/** R22Frame.java
 * Copyright (C) 2016 Hannes Eggert
 * MIT license, see LICENSE file for details
 */
package drone2.gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import drone2.lowlevel.R22LowLevel;

public class R22Frame extends JFrame {
	private final R22LowLevel lowLevel;
	private final JLabel throttleLabel = new JLabel(String.format("%1s [%2$1.1f]", "Throttle", 0.0));
	private final JSlider throttleSlider = new JSlider(0, 100, 100);
	private final JLabel pitchLabel = new JLabel(String.format("%1s [%2$1.1f]", "Pitch", 0.0));
	private final JSlider targetPitchSlider = new JSlider(-100, 100, 0);
	private final JLabel rollLabel = new JLabel(String.format("%1s [%2$1.1f]", "Roll", 0.0));
	private final JSlider targetRollSlider = new JSlider(-100, 100, 0);
	private final JTextField targetHeadingField = new JTextField("0");
	//private final JSlider targetHeadingSlider = new JSlider(-100, 100, 0);
	
	
	
	public R22Frame(R22LowLevel lowLevel)
	{
		super("R22AP");
		this.lowLevel = lowLevel;
		
		this.setLayout(new GridLayout(4, 2));
		this.add(throttleLabel);
		this.add(throttleSlider);
		this.add(pitchLabel);
		this.add(targetPitchSlider);
		this.add(rollLabel);
		this.add(targetRollSlider);
		this.add(new JLabel("target Heading:"));
		this.add(targetHeadingField);
		
		throttleSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				throttleLabel.setText(String.format("%1s [%2$1.1f]", "Throttle", (double)throttleSlider.getValue()));
				lowLevel.setThrottle(throttleSlider.getValue());
			}
		});
		
		targetPitchSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				pitchLabel.setText(String.format("%1s [%2$1.1f]", "Pitch", (double)targetPitchSlider.getValue()));
				lowLevel.setTargetPitch(targetPitchSlider.getValue());
			}
		});
		
		targetRollSlider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				rollLabel.setText(String.format("%1s [%2$1.1f]", "Roll", (double)targetRollSlider.getValue()));
				lowLevel.setTargetRoll(targetRollSlider.getValue());
			}
		});
		
		targetHeadingField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				double d = Double.parseDouble(targetHeadingField.getText());
				lowLevel.setTargetHeading(d);
			}
		});
		this.pack();
		setVisible(true);
	}
}
