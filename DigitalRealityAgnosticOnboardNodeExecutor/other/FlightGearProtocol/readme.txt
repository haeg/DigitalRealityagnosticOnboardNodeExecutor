Place the files 'binIn.xml' and 'binOut.xml' in your Flightgear/data/Protocol directory.

Include the following arguments when starting flightgear:

--generic=socket,out,20,127.0.0.1,5555,udp,binOut
--generic=socket,in,20,,5556,udp,binIn

parameters are: 
type, 
direction, 
rate(packets/sec)
host
port
connectiontype
protocolname